package com.edunetcracker.simulator.websocketserver.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.stereotype.Controller;
import org.springframework.messaging.simp.SimpMessagingTemplate;



@Controller
public class WebSocketController {


    @Autowired
    private SimpMessagingTemplate template;

//    @MessageMapping("/message")
//    @SendTo("/topic/reply")
//    public String processPingMessage(String message) throws Exception {
//
////        wsService.sendPing(1, "stroka");
//        return new Gson().fromJson(message, Map.class).get("name").toString();
//    }

//    @SendTo("/topic/reply")


    @MessageExceptionHandler
    public String handleException(Throwable exception) {
        template.convertAndSend("/errors", exception.getMessage());
        return exception.getMessage();
    }
}
