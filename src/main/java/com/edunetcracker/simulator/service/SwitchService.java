package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.database.repository.networkElementRepository.SwitchRepository;
import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.element.switchNE.Switch;
import com.edunetcracker.simulator.model.executor.Executee;
import com.edunetcracker.simulator.model.executor.ExecuteeRemovedListener;
import com.edunetcracker.simulator.model.port.SwitchPort;
import com.edunetcracker.simulator.model.scene.SceneExecutor;
import com.edunetcracker.simulator.service.configurers.SwitchConfigurer;
import com.edunetcracker.simulator.service.context.ContextService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import lombok.Getter;
import lombok.Synchronized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class SwitchService
        extends DBService<SwitchDTO>
        implements TrackerService<Switch>, ExecuteeRemovedListener {
    private static Logger logger = LoggerFactory.getLogger(SwitchService.class);

    private final SwitchRepository switchRepository;

    private final SwitchConfigurer switchConfigurer;
    private final ContextService contextService;
    private final PortService portService;
    private final LinkService linkService;

    @Getter
    private List<Switch> loadedSwitches = new ArrayList<>();

    private final Object switchRemovedBlocker;
    private boolean switchRemoved;

    public SwitchService(SwitchRepository switchRepository, SwitchConfigurer switchConfigurer, ContextService contextService, PortService portService, LinkService linkService) {
        this.switchRepository = switchRepository;
        this.switchConfigurer = switchConfigurer;
        this.contextService = contextService;
        this.portService = portService;
        this.linkService = linkService;
        switchRemoved = false;
        switchRemovedBlocker = new Object();
    }

    @Override
    public Switch getLoaded(long id) {
        for (Switch switchEl : loadedSwitches) {
            if (switchEl.getData().getIdNE() == id) {
                return switchEl;
            }
        }
        return null;
    }

    @Override
    public SequenceStatus create(SwitchDTO switchDTO) {
        if (null == switchDTO) {
            SequenceStatus.NULL_POINTER.logError("create", "switchEl");
            throw new NullPointerException();
        }
        long instanceId = switchDTO.getIdNE();
        if (instanceId != 0) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("0", "idNE", "switchEl");
            throw new IllegalArgumentException();
        }


        switchConfigurer.givePortsTo(switchDTO);
        switchRepository.save(switchDTO);
        logger.info("Following ports are given to switch with id {}:", switchDTO.getIdNE());
        for (SwitchPort port : switchDTO.getPorts()) {
            logger.info("Port with id {}", port.getId());
        }

        Switch switchEl = new Switch(switchDTO);
        trackRecursively(switchEl);

        SceneService.getInstance().putNetworkElementOnScene(switchDTO);

        return SequenceStatus.OK;
    }

    @Override
    public SwitchDTO update(SwitchDTO switchDTO) {
        if (null == switchDTO) {
            SequenceStatus.NULL_POINTER.logError("update", "switchDTO");
            throw new NullPointerException();
        }

        Switch loadedSwitch = getLoaded(switchDTO.getIdNE());
        SwitchDTO loadedSwitchDTO = null;
        if (null == loadedSwitch) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("Id of a loaded switch", "idNE", "switchDTO");
            return null;
        } else { loadedSwitchDTO = loadedSwitch.getData(); }

        if (null == SceneService.getInstance().getLoaded(loadedSwitchDTO.getSceneId())) {
            SequenceStatus.UNTRACKED_DB_OBJECT
                    .logError("Scene", loadedSwitchDTO.getSceneId() + "(Load the scene first!)");
            return null;
        }
        if (loadedSwitchDTO.getSceneId() != switchDTO.getSceneId()) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE
                    .logWarning("Corresponding scene id", "sceneId", "switchDTO");
            switchDTO.setSceneId(loadedSwitchDTO.getSceneId());
        }
        if (!loadedSwitchDTO.getPhysConfigType().equals(switchDTO.getPhysConfigType())) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE
                    .logWarning("Corresponding physConfigType", "physConfigType", "switchDTO");
            switchDTO.setPhysConfigType(loadedSwitchDTO.getPhysConfigType());
        }
        if (null == switchDTO.getNameNE()) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE
                    .logWarning("New name", "name", "switchDTO. The name will remain unchanged");
            switchDTO.setNameNE(loadedSwitchDTO.getNameNE());
        }

        switchDTO.setSceneDTO(loadedSwitchDTO.getSceneDTO());
        SwitchDTO savedSwitch = switchRepository.save(switchDTO);

        if (savedSwitch != switchDTO) {
            switchDTO.copyAutogeneratedValues(savedSwitch);
        }
        return switchDTO;
    }

    @Override
    public SwitchDTO get (long id) {
        Switch loadedSwitch = getLoaded(id);
        if (null != loadedSwitch) {
            return loadedSwitch.getData();
        }

        Optional<SwitchDTO> switchDTO = switchRepository.findById(id);
        if (!switchDTO.isPresent()) {
            SequenceStatus.NOT_FOUND_IN_DATABASE.logWarning("Switch", id);
            return null;
        }
        return switchDTO.get();
    }

    @Override
    public void trackRecursively(Switch switchEl) {
        if (null == switchEl) {
            SequenceStatus.NULL_POINTER.logError("loadRecursively", "instance");
            throw new NullPointerException();
        }

        List<SwitchPort> switchPorts = switchEl.getData().getPorts();
        for (SwitchPort switchPort : switchPorts) {
            portService.trackRecursively(switchPort);
        }

        if (!loadedSwitches.contains(switchEl)) {
            loadedSwitches.add(switchEl);
        }
    }

    @Override
    public void untrackRecursively(long idNE) {
        Switch instance = getLoaded(idNE);
        if (null == instance) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logWarning("Switch");
            return;
        }

        List<SwitchPort> switchPorts = instance.getData().getPorts();
        for (SwitchPort switchPort : switchPorts) {
            portService.untrackRecursively(switchPort.getId());
        }
        loadedSwitches.remove(instance);
    }

    @Synchronized
    public SequenceStatus stop (Long switchId) {
        Switch switchNE = getLoaded(switchId);
        if (null == switchNE) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        switchNE.setEnabled(false);
//        List<Long> linkIds =
//                switchNE.getPorts().stream()
//                        .filter(switchPort -> null != switchPort.getConnection())
//                        .map(switchPort -> switchPort.getConnection().getLink().getId())
//                        .collect(Collectors.toList());
//        Scene scene = switchNE.getScene();
//
//        scene.getNetworkElements().remove(switchNE);
//        untrackRecursively(switchNE);
//
//        Optional<Switch> switchOpt = switchRepository.findByIdNE(switchId);
//        if (switchOpt.isPresent())
//            switchNE = switchOpt.get();
//        scene.getNetworkElements().add(switchNE);
//        trackRecursively(switchNE);
//
//        for (Long linkId : linkIds) {
//            linkService.linkComps(linkId);
//        }

        return SequenceStatus.OK;
    }

    @Override
    public void drop(SwitchDTO switchDTO) {
        logger.info("Deleting switch with id {} from DB.", switchDTO.getIdNE());
        switchRepository.delete(switchDTO);
    }


    /**
     * Before calling the basic deletion, checks:
     * 1) Whether the Switch with given idNE is loaded,
     * 2) Whether the SceneExecutor the Switch is in is running,
     *
     * If running, waits for the SceneExecutor Thread to remove the Switch from its Executee list.
     *
     * @param idNE Id of a Switch to remove.
     * @return One of these {@code SequenceStatus}es:
     *          UNABLE_TO_REMOVE_IN_RUNTIME, If a trial to remove the Switch in runtime was unsuccessful;
     *          SequenceStatus of {@code delete (long idNE)} otherwise.
     */
    public SequenceStatus deleteSynchronised (long idNE) {
        Switch switchNE = getLoaded(idNE);
        if (null == switchNE) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }

        SceneExecutor sceneExecutor = switchNE.getExecutor();
        boolean sceneIsRunning = sceneExecutor.removeExecutee(switchNE, this);
        if (!sceneIsRunning) {
            try {
                synchronized (switchRemovedBlocker) {
                    while (!switchRemoved) {
                        switchRemovedBlocker.wait();
                    }
                }
            } catch (InterruptedException e) {
                return SequenceStatus.UNABLE_TO_REMOVE_IN_RUNTIME;
            }
        }
        return delete(idNE);
    }

    /**
     * Recursively deletes LOADED Switch with the given id,
     * <b>as if it was in a not-running SceneExecutor Thread</b>.
     *
     * Links connected to the Switch's ports are deleted as well.
     * If Switch is not loaded, does nothing.
     * @return One of these {@code SequenceStatus}es:
     *          UNTRACKED_DB_OBJECT, if the Switch was not loaded,
     *          OK, if deleted successfully.
     */
    public SequenceStatus delete(long idNE) {
        Switch switchNE = getLoaded(idNE);
        if (null == switchNE) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        SceneDTO scene = switchNE.getData().getSceneDTO();
        Collection<Link> sceneLinks = scene.getLinks();
        Collection<SwitchPort> ports = switchNE.getData().getPorts();

        for (SwitchPort port : ports) {
            Link.Connection connection = port.getConnection();
            if (null != connection) {
                Link link = port.getConnection().getLink();
                sceneLinks.remove(link);
                linkService.delete(link.getId());
            }
        }
        scene.getNetworkElementDTOs().remove(switchNE.getData());
        untrackRecursively(idNE);
        drop(switchNE.getData());
        return SequenceStatus.OK;
    }

    @Override
    public void onExecuteeRemoved(Executee executee) {
        synchronized (switchRemovedBlocker) {
            switchRemoved = true;
            switchRemovedBlocker.notify();
        }
    }
}
