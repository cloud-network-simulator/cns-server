package com.edunetcracker.simulator.service;


import com.edunetcracker.simulator.service.status.SequenceStatus;

public abstract class DBService<T> {

    /**
     * Loads a new instance from the database.
     *
     * Think 30 times before using this method for a non-top hierarchy entity.
     *
     * @param id ID of a T to seek
     * @return T the given ID, or nullPrt, if there was no T with such ID in the DB.
     */
    abstract protected T get (long id);

    /**
     * Drops the instance's representation from the database.
     *
     * @param instance Object to delete from database.
     */
    abstract public void drop(T instance);


    /**
     * Creates and fills new DB entry.
     *
     * Use this when you have created a brand new instance, and you know it has
     * never been in the database, and you want to save it the first time.
     * @param instance T to save
     * @return
     */
    abstract public SequenceStatus create (T instance);

    /**
     * Updates the database's row corresponding to the <code>instance</code>.
     *
     * Use this if you know that the object is already saved to database, and
     * you want to update it there.
     * @param instance T to update
     * @return This very same instance.
     */
    abstract public T update (T instance);


}
