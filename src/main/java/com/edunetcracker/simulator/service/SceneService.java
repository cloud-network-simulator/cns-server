package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.database.repository.SceneRepository;
import com.edunetcracker.simulator.model.DTO.element.NetworkElementDTO;
import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.element.switchNE.Switch;
import com.edunetcracker.simulator.model.executor.ExecutorCompleteListener;
import com.edunetcracker.simulator.model.scene.SceneExecutor;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SceneService extends DBService<SceneDTO>
        implements TrackerService<SceneExecutor>, ExecutorCompleteListener<SceneExecutor> {
    private static Logger logger = LoggerFactory.getLogger(SceneService.class);
    //Todo: Need to atomize the operations
    @Getter
    private static SceneService instance;


    private final SceneRepository sceneRepository;

    private final RouterService routerService;
    private final SwitchService switchService;
    private final LinkService linkService;


    private static final String SCENE_DEFAULT_NAME = "Sample Text";

    @Getter
    private List<SceneExecutor> loadedScenes = new ArrayList<>();

    @Autowired
    public SceneService(SceneRepository sceneRepository, RouterService routerService, SwitchService switchService, LinkService linkService) {
        if (null != instance) {
            logger.error("Detected a trial to initialize another instance of the service!");
        }
        instance = this;

        this.sceneRepository = sceneRepository;
        this.routerService = routerService;
        this.switchService = switchService;
        this.linkService = linkService;
    }

    public SequenceStatus delete(long id) {
        SceneExecutor scene = getLoaded(id);
        if (null != scene) {
            if (scene.isStarted()) {
                logger.warn("Detected a trial to delete a running scene!");
                return SequenceStatus.UNABLE_TO_REMOVE_IN_RUNTIME;
            }
            untrackRecursively(id);
            Collection<Link> links = scene.getSceneDTO().getLinks();
            Collection<NetworkElementDTO> networkElements = scene.getSceneDTO().getNetworkElementDTOs();
            for (Link link : links) {
                linkService.delete(link.getId());
            }
            for (NetworkElementDTO networkElement : networkElements) {
                if (networkElement instanceof SwitchDTO) {
                    switchService.delete(networkElement.getIdNE());
                }
                if (networkElement instanceof RouterDTO) {
                    routerService.delete(networkElement.getIdNE());
                }
            }
        }
        //ToDo(Wisp): Check whether there are any scene objects left with the SceneId == id.
        sceneRepository.deleteById(id);
        return SequenceStatus.OK;
    }

    public SceneDTO change(int id, SceneDTO sceneDTO){
        SceneDTO sceneDTOToChange = loadedScenes.get(id).getSceneDTO();
        if (null == sceneDTOToChange) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            return null;
        }
        if (null == sceneDTO) {
            SequenceStatus.NULL_POINTER.logError("change", "sceneDTO");
            return null;
        }
        sceneDTOToChange.setName(sceneDTO.getName());
        sceneDTOToChange.setLinks(sceneDTO.getLinks());
        sceneDTOToChange.setNetworkElementDTOs(sceneDTO.getNetworkElementDTOs());
        return sceneDTOToChange;
    }

    private void instantiateNetworkElements (SceneExecutor sceneExecutor) {
        assert null != sceneExecutor;
        SceneDTO sceneDTO = sceneExecutor.getSceneDTO();
        for (NetworkElementDTO networkElementDTO : sceneDTO.getNetworkElementDTOs()) {
            NetworkElement networkElement = networkElementDTO.getNetworkElement();
            if (null == networkElement) {
                logger.error("Notices unprepared networkElement (id {}) during preparation of SceneExecutor (DTOid {}) thread.",
                                                                 networkElementDTO.getIdNE(),                sceneDTO.getId());
            } else {
                sceneExecutor.addExecutee(networkElement);
            }
        }
    }


    public SceneExecutor load (long id) {
        SceneExecutor loadedScene = getLoaded(id);
        if (null != loadedScene) {
            return loadedScene;
        }

        Optional<SceneDTO> sceneDTO = sceneRepository.findById(id);
        if (!sceneDTO.isPresent()) {
            SequenceStatus.NOT_FOUND_IN_DATABASE.logWarning("Scene", id);
            return null;
        }
        SceneExecutor newSceneExecutor = new SceneExecutor(sceneDTO.get(), this);
        trackRecursively(newSceneExecutor);
        instantiateNetworkElements(newSceneExecutor);

        return newSceneExecutor;
    }

    SceneExecutor load (String name) {
        if (null == name) {
            SequenceStatus.NULL_POINTER.logError("getByName", "name");
            throw new NullPointerException();
        }
        Optional<SceneDTO> sceneDTO = sceneRepository.findByName(name);
        if (sceneDTO.isPresent()) {
            return load(sceneDTO.get().getId());
        }
        else {
            return null;
        }
    }

    @Override
    public SceneExecutor getLoaded(long id) {
        for (SceneExecutor scene : loadedScenes) {
            if (scene.getSceneDTO().getId() == id) {
                return scene;
            }
        }
        return null;
    }

    private SceneExecutor getLoaded (String name) {
        if (null == name) {
            SequenceStatus.NULL_POINTER.logError("getLoadedByName", "name");
            throw new NullPointerException();
        }
        for (SceneExecutor scene : loadedScenes) {
            if (name.equals(scene.getName())) {
                return scene;
            }
        }
        return null;
    }

    /**
     * Additionaly to the base's documentation (see {@link TrackerService}), adds
     * executees to the sceneExecutor's executee set.
     *
     * @param sceneExecutor
     */
    @Override
    public void trackRecursively (SceneExecutor sceneExecutor) {
        if (null == sceneExecutor) {
            throw new NullPointerException();
        }
        if (loadedScenes.contains(sceneExecutor)) {
            logger.info("SceneService already tracks scene with id {}", sceneExecutor.getSceneDTO().getId());
            return;
        }

        if (null != sceneExecutor.getSceneDTO().getNetworkElementDTOs()) {
            for (NetworkElementDTO ne : sceneExecutor.getSceneDTO().getNetworkElementDTOs()) {
                ne.setSceneId(sceneExecutor.getSceneDTO().getId());
                if (ne instanceof RouterDTO) {
                    Router router = new Router((RouterDTO)ne);
                    routerService.trackRecursively(router);
                }
                if (ne instanceof SwitchDTO) {
                    Switch switchEl = new Switch((SwitchDTO)ne);
                    switchService.trackRecursively(switchEl);
                }
            }
        }
        if (null != sceneExecutor.getSceneDTO().getLinks()) {
            for (Link link : sceneExecutor.getSceneDTO().getLinks()) {
                logger.info("Linking ports {} and {}.", link.getPortAid(), link.getPortZid());
                linkService.linkComps(link);
            }
        }

        loadedScenes.add(sceneExecutor);
        logger.info("Added scene with id {} to a track list: {}.", sceneExecutor.getSceneDTO().getId(), null != getLoaded(sceneExecutor.getSceneDTO().getId()));
    }

    @Override
    public void untrackRecursively(long id) {
        SceneExecutor instance = getLoaded(id);
        if (null == instance) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logWarning("Scene");
            return;
        }

        Collection<NetworkElementDTO> networkElementDTOs = instance.getSceneDTO().getNetworkElementDTOs();
        if (null != networkElementDTOs) {
            for (NetworkElementDTO ne : instance.getSceneDTO().getNetworkElementDTOs()) {
                if (ne instanceof RouterDTO) routerService.untrackRecursively(ne.getIdNE());
                if (ne instanceof SwitchDTO) switchService.untrackRecursively(ne.getIdNE());
            }
        }

        Collection<Link> links = instance.getSceneDTO().getLinks();
        if (null != links) {
            for (Link link : links) {
                linkService.untrackRecursively(link.getId());
            }
        }

        loadedScenes.remove(instance);
    }

    public Map<Long, String> getSceneNames () {
        List<SceneDTO> sceneDTOList = sceneRepository.findAll();
        Map<Long, String> namesMap =
                sceneDTOList.stream()
                .collect(Collectors.toMap(SceneDTO::getId, SceneDTO::getName));
        return namesMap;
    }

    @Override
    public SceneDTO get (long id) {
        SceneExecutor loadedScene = getLoaded(id);
        if (null != loadedScene) {
            if (null == loadedScene.getSceneDTO()) {
                throw new NullPointerException(SequenceStatus.UNEXPECTED_FIELD_VALUE
                        .getStringBody("Not null", "sceneDTO", "loadedScene"));
            }
            return loadedScene.getSceneDTO();
        }

        Optional<SceneDTO> scene = sceneRepository.findById(id);
        if (!scene.isPresent()) {
            SequenceStatus.NOT_FOUND_IN_DATABASE.logWarning("Scene", id);
            return null;
        }
        return scene.get();
    }

    @Override
    public SequenceStatus create (SceneDTO sceneDTO) {
        if (null == sceneDTO) {
            SequenceStatus.NULL_POINTER.logError("saveNew", "scene");
            return SequenceStatus.NULL_POINTER;
        }
        long instanceId = sceneDTO.getId();
        if (instanceId != 0) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("0", "ID", "Scene DTO");
        }

        if (null == sceneDTO.getName()) {
            sceneDTO.setName(SCENE_DEFAULT_NAME);
        }
        update(sceneDTO);
        return SequenceStatus.OK;
    }

    @Override
    public SceneDTO update(SceneDTO sceneDTO) {
        if (null == sceneDTO) {
            SequenceStatus.NULL_POINTER.logError("update", "sceneDTO");
            throw new NullPointerException();
        }
        SceneDTO savedSceneDTO = sceneRepository.save(sceneDTO);

        if (savedSceneDTO != sceneDTO) {
            sceneDTO.copyAutogeneratedValues(savedSceneDTO);
        }
        return sceneDTO;
    }

    /**
     * Updates loaded scene in a database.
     * If a scene with the id wasn't loaded, nothing happens.
     * @param id Id of a scene to change.
     */
    public SceneDTO update (Long id) {
        if (null == id) {
            throw new NullPointerException("Received null pointer as id.");
        }
        logger.info("Updating scene with ID{}", id);
        SceneExecutor sceneExecutor = getLoaded(id);
        if (null == sceneExecutor) {
            return null;
        }
        return update(sceneExecutor.getSceneDTO());
    }

    @Override
    public void drop(SceneDTO sceneDTO) {
        sceneRepository.delete(sceneDTO);
    }

    /**
     * The logic of this method depends on whether the scene would be not loaded, loaded or launched.
     *
     * @param networkElementDTO New networkElement to add on scene.
     */
    public SequenceStatus putNetworkElementOnScene(NetworkElementDTO networkElementDTO) {
        if (null == networkElementDTO) {
            throw new NullPointerException(SequenceStatus.NULL_POINTER.getStringBody("putRouterOnScene", "routerDTO"));
        }

        SceneExecutor loadedScene = getLoaded(networkElementDTO.getSceneId());
        SceneDTO sceneDTO = null;

        if (null != loadedScene) {
            //ToDo: Trial to get networkElement from DTO is deprecated
            //      if TrackerServices of Router and Switch are deprecated.
            NetworkElement networkElement = networkElementDTO.getNetworkElement();
            if (null == networkElement) {
                if (networkElementDTO instanceof RouterDTO) {
                    RouterDTO routerDTO = (RouterDTO) networkElementDTO;
                    networkElement = new Router(routerDTO);
                    routerDTO.setRouter((Router) networkElement);
                }
                if (networkElementDTO instanceof SwitchDTO) {
                    SwitchDTO switchDTO = (SwitchDTO) networkElementDTO;
                    networkElement = new Switch(switchDTO);
                    switchDTO.setSwitchEl((Switch) networkElement);
                }
                if (null == networkElement) {
                    throw new NotImplementedException();
                }
            }

            loadedScene.addExecutee(networkElement);
            sceneDTO = loadedScene.getSceneDTO();
        } else {
            sceneDTO = get(networkElementDTO.getSceneId());
            if (null == sceneDTO) {
                return SequenceStatus.NOT_FOUND_IN_DATABASE;
            }
        }
        networkElementDTO.setSceneDTO(sceneDTO);
        sceneDTO.addNetworkElement(networkElementDTO);

        update(sceneDTO);

        return SequenceStatus.OK;
    }


    @Override
    public void onExecutorComplete(SceneExecutor sceneExecutor) {
        assert null != sceneExecutor;
        if (sceneExecutor != getLoaded(sceneExecutor.getSceneDTO().getId())) {
            throw new UnknownError(SequenceStatus.DUPLICATE_ID
                    .getStringBody("Scene", sceneExecutor.getSceneDTO().getId()));
        }

        SceneExecutor newSceneExecutor = new SceneExecutor(sceneExecutor.getSceneDTO(), this);
        loadedScenes.remove(sceneExecutor);
        loadedScenes.add(newSceneExecutor);
        instantiateNetworkElements(newSceneExecutor);

        logger.info("Successfully updated sceneExecutor in a SceneService tracker.");
    }
}
