package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.database.repository.LinkRepository;
import com.edunetcracker.simulator.model.DTO.userCommand.PingCommandDTO;
import com.edunetcracker.simulator.model.DTO.element.NetworkElementDTO;
import com.edunetcracker.simulator.model.DTO.userCommand.UserCommandDTO;
import com.edunetcracker.simulator.model.DTO.userCommand.traffic.TrafficDTO;
import com.edunetcracker.simulator.model.context.NEContext;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.scene.SceneExecutor;
import com.edunetcracker.simulator.service.context.ContextService;
import com.edunetcracker.simulator.service.routingService.IpService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//import static com.edunetcracker.simulator.model.context.TrafficContext.TrafficType.SIMPLE;


@Service
public class SimulatorService {
    private static Logger logger = LoggerFactory.getLogger(SimulatorService.class);

    private final LinkRepository linkRepository;
    private final RouterService routerService;
    private final SwitchService switchService;
    private final ContextService contextService;
    private final SceneService sceneService;

    @Autowired
    public SimulatorService(LinkRepository linkRepository, RouterService routerService, SwitchService switchService, ContextService contextService, SceneService sceneService) {
        this.linkRepository = linkRepository;
        this.routerService = routerService;
        this.switchService = switchService;
        this.contextService = contextService;
        this.sceneService = sceneService;
    }


    public ResponseEntity<List<NetworkElement>> getComps () {
//        чтение из бд сохраненных параметров
// fixme: wrong method, it is necessary to return object with all information about Scene

        List<NetworkElement> comps = new ArrayList<>();

        return ResponseEntity.status(HttpStatus.OK).body(comps);
    }


    public ResponseEntity startComponent(Long id) {
        if (null == id) {
            SequenceStatus.NULL_POINTER.logError("startComponent", "id");
            return null;
        }
        Gson gson = new Gson();
        NetworkElement ne = switchService.getLoaded(id);
        if (null != ne) {
            ne.start();
            return ResponseEntity.ok().body(gson.toJson("Started switch successfully."));
        }
        ne = routerService.getLoaded(id);
        if (null != ne) {
            ne.start();
            return ResponseEntity.ok().body(gson.toJson( "Started router successfully."));
        }
        logger.error("Tried to start nonexisting NetworkElement with id {}", id);
        return ResponseEntity.badRequest()
                .body(gson.toJson(String.format("Tried to start nonexisting NetworkElement with id %d", id)));
    }

    public ResponseEntity startAllComponents(Long sceneId) {
        if (null == sceneId) {
            SequenceStatus.NULL_POINTER.logError("startAllComponents", "sceneId");
            return null;
        }
        SceneExecutor scene = sceneService.getLoaded(sceneId);
        if (null == scene || null == scene.getSceneDTO()) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            return null;
        }
        List<NetworkElementDTO> networkElementDTOs = scene.getSceneDTO().getNetworkElementDTOs();

        for (NetworkElementDTO neDTO : networkElementDTOs) {
            neDTO.getNetworkElement().start();
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson("All components have been started"));
    }


    public ResponseEntity startComponents (List<Integer> ids) {
        for (Integer id : ids) {
            NetworkElement ne = switchService.getLoaded(id);
            if (null == ne) {
                ne = routerService.getLoaded(id);
            }
            if (null != ne) {
                ne.setEnabled(true);
                ne.start();
            } else {
                logger.error("Tried to start nonexisting NetworkElement with id {}", id);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body("Request components have been started");
    }

    public ResponseEntity pauseComponent(Long id) {
        if (null == id) {
            SequenceStatus.NULL_POINTER.logError("stopComponent", "id");
            return null;
        }
        Gson gson = new Gson();
        NetworkElement ne = switchService.getLoaded(id);
        if (null != ne) {
            switchService.stop(ne.getData().getIdNE());
            return ResponseEntity.ok().body(gson.toJson("Stopped switch successfully."));
        }
        ne = routerService.getLoaded(id);
        if (null != ne) {
            routerService.stop(ne.getData().getIdNE());
            return ResponseEntity.ok().body(gson.toJson( "Stopped router successfully."));
        }
        logger.error("Tried to stop nonexisting NetworkElement with id {}", id);
        return ResponseEntity.badRequest()
                .body(gson.toJson(String.format("Tried to stop nonexisting NetworkElement with id %d", id)));
    }

    /**
     * Launches simulation of network on a scene in a different thread.
     *
     * @param sceneId Id of a scene, which thread to launch.
     * @return Prepared response entity with one of those HTTP statuses:
     *          OK,                     if everything went ok,
     *          ExpectationFailed,      if there was no such id,
     *          ALREADY_REPORTED,       if the simulation is already started.
     */
    public ResponseEntity startSimulation (long sceneId) {
        SceneExecutor sceneExecutor = sceneService.getLoaded(sceneId);
        if (null == sceneExecutor) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logWarning("Scene", sceneId);
            return ResponseEntity.status(SequenceStatus.UNTRACKED_DB_OBJECT.getHttpStatus())
                                 .body  (SequenceStatus.UNTRACKED_DB_OBJECT.getStringBody("Scene"));
        }
        if (sceneExecutor.isStarted()) {
            SequenceStatus.SIMULATION_ALREADY_STARTED.logWarning(sceneId);
            return ResponseEntity.status(SequenceStatus.SIMULATION_ALREADY_STARTED.getHttpStatus())
                                 .body  (SequenceStatus.SIMULATION_ALREADY_STARTED.getStringBody(sceneId));
        }

        sceneExecutor.start();
        return ResponseEntity.ok("ok");
    }

    /**
     * Tells the scene's process to make its final loop and stop executing.
     *
     * @param sceneId Id of a scene, which process to stop.
     * @return Prepared response entity with one of those HTTP statuses:
     *          OK,                  if everything went ok,
     *          ExpectationFailed,   if there was no such id.
     */
    public ResponseEntity stopSimulation (long sceneId) {
        SceneExecutor sceneExecutor = sceneService.getLoaded(sceneId);
        if (null == sceneExecutor) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            return ResponseEntity.status(SequenceStatus.UNTRACKED_DB_OBJECT.getHttpStatus())
                                 .body  (SequenceStatus.UNTRACKED_DB_OBJECT.getHttpBody());
        }
        sceneExecutor.lastLoop();
        return ResponseEntity.ok(SequenceStatus.OK);
    }

    public ResponseEntity pauseAllComponents(Long sceneId) {
        if (null == sceneId) {
            SequenceStatus.NULL_POINTER.logError("stopAllComponents", "sceneId");
            return null;
        }
        SceneExecutor sceneExecutor = sceneService.getLoaded(sceneId);
        if (null == sceneExecutor) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Scene");
            return null;
        }
        List<NetworkElementDTO> networkElements = sceneExecutor.getSceneDTO().getNetworkElementDTOs();

        for (NetworkElementDTO ne : networkElements) {
            ne.getNetworkElement().setEnabled(false);
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson("All components have been stopped"));
    }

    public ResponseEntity stopComponents (List<Integer> ids) {

        return ResponseEntity.status(HttpStatus.OK).body("Request components have been stopped");
    }

    /**
     * Creates a ping context for a router.
     * (All the work of finding necessary port is on context's shoulders).
     * @param pingCommandDTO
     * @return
     */
    public SequenceStatus ping (PingCommandDTO pingCommandDTO) {
        Router router = routerService.getLoaded(pingCommandDTO.getIdNE());
        if (null == router) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logWarning("Router", pingCommandDTO.getIdNE());
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        logger.trace("Requested router id: {}. Found: {}", pingCommandDTO.getIdNE(), router);

        NEContext ping = contextService.ping(router.getSessionId(), null, IpService.intFromString(pingCommandDTO.getDestinationIp()));
        router.getContexts().add(ping);

        return SequenceStatus.OK;
    }

    /**
     * An Entity to test traffic function
     * GeneratedConfig is a DAO, where we write our params (so that not to
     * send them separately each time, but rather collect in one class
     *
     * @param generatedConfig Class of parameters for traffic
     * @return ok statement if everything works fine
     */
    public ResponseEntity traffic (TrafficDTO generatedConfig){
        Router router = routerService.getLoaded(generatedConfig.getIdNE());

        NEContext traffic = contextService.traffic(generatedConfig);
        router.getContexts().add(traffic);

        return ResponseEntity.ok(traffic.getId());
    }

    /**
     * Puts a new user command into the queue of a router with id determined
     * in DTO.
     *
     * @param userCommandDTO Command to put in a queue.
     * @return One of the following response entities:
     * OK: Added command successfully
     * ResponseEntity determined by UNTRACKED_DB_OBJECT SequenceStatus.
     */
    public ResponseEntity addUserCommand(UserCommandDTO userCommandDTO) {
        Router router = routerService.getLoaded(userCommandDTO.getIdNE());
        if (null == router) {
            return ResponseEntity.status(SequenceStatus.UNTRACKED_DB_OBJECT.getHttpStatus())
                    .body(SequenceStatus.UNTRACKED_DB_OBJECT.getStringBody("Router"));
        }

        router.getUserInput().add(userCommandDTO);

        return ResponseEntity.ok().body("Added command successfully.");
    }
}
