package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.service.status.SequenceStatus;

//ToDo(Refactoring): The engines will be the trackers.
// One and only tracker of the service level will be the SceneTracker.
/**
 * Tracks instances of T so that the application doesn't have duplicate scenes, routers, links e.t.c.
 * @param <T>
 */
public interface TrackerService<T> {

    /**
     * Tries to find an already loaded object.
     * @param id ID of an object to find
     * @return Object with the given ID, or null if fails to find it in the tracklist.
     */
    T getLoaded (long id);


    /**
     * Adds an instance and all it's underling trackable objects to services' track lists.
     * Method is used by <code>load</code> method.
     * @param instance T to 'load'.
     */
    void trackRecursively (T instance);

    /**
     * Removes an object and all it's underling trackable objects from services' track lists.
     * @param id ID of an object to unload.
     */
    void untrackRecursively (long id);
}
