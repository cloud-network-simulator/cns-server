package com.edunetcracker.simulator.service;


import com.edunetcracker.simulator.database.repository.networkElementRepository.RouterRepository;
import com.edunetcracker.simulator.model.DTO.userCommand.SetIpCommandDTO;
import com.edunetcracker.simulator.model.DTO.userCommand.IpRouteCommandDTO;
import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.executor.Executee;
import com.edunetcracker.simulator.model.executor.ExecuteeRemovedListener;
import com.edunetcracker.simulator.model.port.RouterPort;
import com.edunetcracker.simulator.model.element.router.routing.routingTable.RoutingTable;
import com.edunetcracker.simulator.model.scene.SceneExecutor;
import com.edunetcracker.simulator.service.configurers.RouterConfigurer;

import com.edunetcracker.simulator.service.context.ContextService;
import com.edunetcracker.simulator.service.routingService.IpService;
import com.edunetcracker.simulator.service.routingService.RoutingTableService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.Synchronized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RouterService
        extends DBService<RouterDTO>
        implements TrackerService<Router>, ExecuteeRemovedListener {
    private static Logger logger = LoggerFactory.getLogger(RouterService.class);

    private final WSService wsService;
    private final ContextService contextService;
    private final RouterRepository routerRepository;
    private final RouterConfigurer routerConfigurer;
    private final PortService portService;
    private final RoutingTableService routingTableService;
    private final LinkService linkService;
    @Getter
    private static RouterService instance;

    @Getter
    private List<Router> loadedRouters = new ArrayList<>();

    private final Object routerRemovedBlocker;
    private boolean routerRemoved;

    @Autowired
    public RouterService(WSService wsService, ContextService contextService, RouterRepository routerRepository, RouterConfigurer routerConfigurer, PortService portService, RoutingTableService routingTableService, LinkService linkService) {
        this.wsService = wsService;
        this.contextService = contextService;
        this.routerRepository = routerRepository;
        this.routerConfigurer = routerConfigurer;
        this.portService = portService;
        this.routingTableService = routingTableService;
        this.linkService = linkService;

        routerRemovedBlocker = new Object();
        routerRemoved = false;
        RouterService.instance = this;
    }

    @Override
    public Router getLoaded(long id) {
        for (Router router : loadedRouters) {
            if (router.getData().getIdNE() == id) {
                return router;
            }
        }
        return null;
    }

    @Override
    public RouterDTO get (long id) {
        Router loadedRouter = getLoaded(id);
        if (null != loadedRouter) {
            return loadedRouter.getData();
        }

        Optional<RouterDTO> router = routerRepository.findById(id);
        if (!router.isPresent()) {
            SequenceStatus.NOT_FOUND_IN_DATABASE.logWarning("Router", id);
            return null;
        }
        return router.get();
    }

    @Override
    public SequenceStatus create (RouterDTO routerDTO) {
        if (null == routerDTO) {
            SequenceStatus.NULL_POINTER.logError("saveNew", "router");
            return SequenceStatus.NULL_POINTER;
        }
        long instanceId = routerDTO.getIdNE();
        if (instanceId != 0) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("0", "idNE", "router");
            throw new IllegalArgumentException();
        }

        routerConfigurer.givePortsTo(routerDTO);
        routerRepository.save(routerDTO);
        logger.info("Following ports are given to router with id {}:", routerDTO.getIdNE());
        for (RouterPort port : routerDTO.getPorts()) {
            logger.info("Port with id {}", port.getId());
        }

        Router router = new Router(routerDTO);
        trackRecursively(router);

        SceneService.getInstance().putNetworkElementOnScene(routerDTO);

        return SequenceStatus.OK;
    }

    @Override
    public RouterDTO update(RouterDTO routerDTO) {
        if (null == routerDTO) {
            SequenceStatus.NULL_POINTER.logError("update", "routerDTO");
            throw new NullPointerException();
        }

        Router loadedRouter = getLoaded(routerDTO.getIdNE());
        RouterDTO loadedRouterDTO = null;
        if (null == loadedRouter) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("Id of a loaded router", "idNE", "routerDTO");
            return null;
        } else { loadedRouterDTO = loadedRouter.getData(); }

        if (null == SceneService.getInstance().getLoaded(loadedRouterDTO.getSceneId())) {
            SequenceStatus.UNTRACKED_DB_OBJECT
                    .logError("Scene", loadedRouterDTO.getSceneId() + "(Load the scene first!)");
            return null;
        }
        if (loadedRouterDTO.getRoutingTable().getId() != routerDTO.getRoutingTable().getId()) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE
                    .logWarning("Routing table with a corresponding id", "routingTable", "routerDTO");
            routerDTO.setRoutingTable(loadedRouterDTO.getRoutingTable());
        }
        if (loadedRouterDTO.getSceneId() != routerDTO.getSceneId()) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE
                    .logWarning("Corresponding scene id", "sceneId", "routerDTO");
            routerDTO.setSceneId(loadedRouterDTO.getSceneId());
        }
        if (!loadedRouterDTO.getPhysConfigType().equals(routerDTO.getPhysConfigType())) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE
                    .logWarning("Corresponding physConfigType", "physConfigType", "routerDTO");
            routerDTO.setPhysConfigType(loadedRouterDTO.getPhysConfigType());
        }
        if (null == routerDTO.getNameNE()) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE
                    .logWarning("New name", "name", "routerDTO. The name will remain unchanged");
            routerDTO.setNameNE(loadedRouterDTO.getNameNE());
        }

        routerDTO.setSceneDTO(loadedRouterDTO.getSceneDTO());
        routerDTO.setRoutingTable(loadedRouterDTO.getRoutingTable());
        RouterDTO savedRouter = routerRepository.save(routerDTO);
        logger.warn("MIND THAT THE ROUTING TABLE IS IMMUTABLE WITH THIS METHOD!!!");
        if (savedRouter != routerDTO) {
            routerDTO.copyAutogeneratedValues(savedRouter);
        }

        return routerDTO;
    }

    @Override
    public void trackRecursively(Router router) {
        if (null == router) {
            SequenceStatus.NULL_POINTER.logError("trackRecursively", "instance");
            throw new NullPointerException();
        }

        List<RouterPort> routerPorts = router.getData().getPorts();
        for (RouterPort routerPort : routerPorts) {
            portService.trackRecursively(routerPort);
        }
        routingTableService.trackRecursively(router.getData().getRoutingTable());

        if (!loadedRouters.contains(router)) {
            loadedRouters.add(router);
        }
    }

    @Override
    public void untrackRecursively(long id) {
        Router instance = getLoaded(id);
        if (null == instance) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("RouterService");
            return;
        }
        List<RouterPort> routerPorts = instance.getData().getPorts();
        for (RouterPort routerPort : routerPorts) {
            portService.untrackRecursively(routerPort.getId());
        }
        routingTableService.untrackRecursively(instance.getData().getRoutingTable().getId());
        loadedRouters.remove(instance);
    }

    @Override
    public void drop (RouterDTO routerDTO) {
        logger.info("Deleting router with id {} from DB...", routerDTO.getIdNE());
        routerRepository.delete(routerDTO);
    }

    @Synchronized
    public SequenceStatus stop (Long routerId) {
        Router router = getLoaded(routerId);
        if (null == router) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        router.setEnabled(false);
        //ToDo: Find out whether this code is required
//        List<Long> linkIds =
//                router.getPorts().stream()
//                        .filter(routerPort -> null != routerPort.getConnection())
//                        .map(routerPort -> routerPort.getConnection().getLink().getId())
//                        .collect(Collectors.toList());
//        Scene scene = router.getData().getScene();
//
//        scene.getNetworkElements().remove(router);
//        untrackRecursively(router);
//
//        Optional<RouterDTO> routerDtoOpt = routerRepository.findByIdNE(routerId);
//        if (routerDtoOpt.isPresent())
//            router = routerDtoOpt.get();
//        scene.getNetworkElements().add(router);
//        trackRecursively(router);
//
//        for (Long linkId : linkIds) {
//            linkService.linkComps(linkId);
//        }

        return SequenceStatus.OK;
    }


    /**
     * Before calling the basic deletion, checks:
     * 1) Whether the Router with given idNE is loaded,
     * 2) Whether the SceneExecutor the Router is in is running,
     *
     * If running, waits for the SceneExecutor Thread to remove the router from its Executee list.
     *
     * @param idNE Id of a router to remove.
     * @return One of these {@code SequenceStatus}es:
     *          UNABLE_TO_REMOVE_IN_RUNTIME, If a trial to remove the router in runtime was unsuccessful;
     *          SequenceStatus of {@code delete (long idNE)} otherwise.
     */
    public SequenceStatus deleteSynchronised (long idNE) {
        Router router = getLoaded(idNE);
        if (null == router) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }

        SceneExecutor sceneExecutor = router.getExecutor();
        boolean sceneIsRunning = sceneExecutor.removeExecutee(router, this);
        if (!sceneIsRunning) {
            try {
                synchronized (routerRemovedBlocker) {
                    while (!routerRemoved) {
                        routerRemovedBlocker.wait();
                    }
                }
            } catch (InterruptedException e) {
                return SequenceStatus.UNABLE_TO_REMOVE_IN_RUNTIME;
            }
        }
        return delete(idNE);
    }


    /**
     * Recursively deletes LOADED router with the given id,
     * <b>as if it was in a not-running SceneExecutor Thread</b>.
     *
     * Links connected to the router's ports are deleted as well.
     * If router is not loaded, does nothing.
     * @return One of these {@code SequenceStatus}es:
     *          UNTRACKED_DB_OBJECT, if the router was not loaded,
     *          OK, if deleted successfully.
     */
    public SequenceStatus delete (long idNE) {
        Router router = getLoaded(idNE);
        if (null == router) {
            return SequenceStatus.UNTRACKED_DB_OBJECT;
        }
        SceneDTO scene = router.getData().getSceneDTO();
        Collection<Link> sceneLinks = scene.getLinks();
        Collection<RouterPort> ports = router.getData().getPorts();

        for (RouterPort port : ports) {
            Link.Connection connection = port.getConnection();
            if (null != connection) {
                Link link = port.getConnection().getLink();
                sceneLinks.remove(link);
                linkService.delete(link.getId());
            }
        }
        scene.getNetworkElementDTOs().remove(router.getData());
        untrackRecursively(idNE);
        drop(router.getData());
        return SequenceStatus.OK;
    }

    public ResponseEntity setIpAddress (SetIpCommandDTO setIpCommandDTO) {
        return setIpAddress(setIpCommandDTO.getIdNE(), setIpCommandDTO.getPortNumber(),
                setIpCommandDTO.getIp(), setIpCommandDTO.getMask());
    }

    public ResponseEntity setIpAddress (Long routerId, Integer portNumber, String ip, String mask) {
        Router router = getLoaded(routerId);
        if (null == router) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body(String.format("Loaded router with id %d couldn't have been found.", routerId));
        }
        RouterPort port = router.getData().getPort(portNumber);
        if (null == port) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body(String.format("Port with number %d couldn't have been found.", portNumber));
        }

        SequenceStatus sequenceStatus =
                port.setIpAddress(ip, mask);
        if (SequenceStatus.NULL_POINTER == sequenceStatus) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("IP was null.");
        }
        RoutingTableService.routeDirectConnection(port);

        Gson gson = new Gson();
        logger.info("RouterID {}, port {} (portId {}), ip {}.", routerId, portNumber, port.getId(), IpService.stringFromInt(router.getData().getPort(portNumber).getIp()));
        return ResponseEntity.ok().body(gson.toJson("ok"));
    }

    public RoutingTable routeStatic (IpRouteCommandDTO ipRouteCommandDTO) {
        Router router = getLoaded(ipRouteCommandDTO.getIdNE());
        if (null == router) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("Router");
            throw new RuntimeException();
        }
        routingTableService.routeStatic(router.getData().getRoutingTable(),
                                        IpService.intFromString(ipRouteCommandDTO.getIp()),
                                        IpService.intFromString(ipRouteCommandDTO.getMask()),
                                        IpService.intFromString(ipRouteCommandDTO.getNextHop()));

        return router.getData().getRoutingTable();
    }

    @Override
    public void onExecuteeRemoved(Executee executee) {
        synchronized (routerRemovedBlocker) {
            routerRemoved = true;
            routerRemovedBlocker.notify();
        }
    }
}
