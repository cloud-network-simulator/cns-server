package com.edunetcracker.simulator.service;

import com.edunetcracker.simulator.model.DTO.SocketMessageDTO;
import com.google.gson.Gson;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class WSService {

    @Getter
    private static WSService instance;

    public WSService() {
        instance = this;
    }


    @Autowired
    private SimpMessagingTemplate template;

    public void send(SocketMessageDTO message) {
        //System.out.println("Ya websocketService sent" + message.type);
       // String mess = new Gson().toJson(message);
        String destination = "/user/{userId}/" + message.sessionId;
        this.template.convertAndSend(destination,message);
    }

}
