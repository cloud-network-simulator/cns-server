package com.edunetcracker.simulator.service.routingService;

import com.edunetcracker.simulator.database.repository.routingRepository.RoutingTableRepository;
import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.port.RouterPort;
import com.edunetcracker.simulator.model.element.router.routing.routingTable.RoutingTable;
import com.edunetcracker.simulator.model.element.router.routing.routingTableEntry.RoutingTableEntry;
import com.edunetcracker.simulator.model.element.router.routing.routingTableEntry.RoutingTableEntryFactory;
import com.edunetcracker.simulator.service.DBService;
import com.edunetcracker.simulator.service.TrackerService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoutingTableService extends DBService<RoutingTable> implements TrackerService<RoutingTable> {
    private static Logger logger = LoggerFactory.getLogger(RoutingTableService.class);

    @Autowired
    private RoutingTableRepository routingTableRepository;

    @Getter
    private List<RoutingTable> loadedRoutingTables = new ArrayList<>();

    @Override
    public RoutingTable getLoaded(long id) {
        for (RoutingTable table : loadedRoutingTables) {
            if (table.getId() == id) {
                return table;
            }
        }
        return null;
    }

    @Override
    public SequenceStatus create(RoutingTable routingTable) {
        if (null == routingTable) {
            SequenceStatus.NULL_POINTER.logError("saveRoutingTable", "routingTable");
            throw new NullPointerException();
        }
        long instanceId = routingTable.getId();
        if (instanceId != 0) {
            SequenceStatus.UNEXPECTED_FIELD_VALUE.logError("0", "id", "routingTable");
            throw new ArrayStoreException();
        }
        trackRecursively(routingTable);
        update(routingTable);
        return SequenceStatus.OK;
    }

    @Override
    public RoutingTable get (long id) {
        Optional<RoutingTable> newlyLoaded = routingTableRepository.findById(id);
        if (!newlyLoaded.isPresent()) {
            SequenceStatus.NOT_FOUND_IN_DATABASE.logWarning("RoutingTable", id);
            return null;
        }
        return newlyLoaded.get();
    }

    @Override
    public RoutingTable update(RoutingTable routingTable) {
        if (null == routingTable) {
            SequenceStatus.NULL_POINTER.logError("updateInDB", "routingTable");
            throw new NullPointerException();
        }
        if (null == getLoaded(routingTable.getId())) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logError("RoutingTable");
            trackRecursively(routingTable);
        }
        RoutingTable savedTable = routingTableRepository.save(routingTable);

        if (savedTable != routingTable) {
            routingTable.copyAutogeneratedValues(savedTable);
        }
        return routingTable;
    }


    /**
     * Returns local port route and directly connected subnetwork route.
     * @param port
     * @return Array of two RoutingTableEntries: 0 is Local Port entry, 1 is Connected Subnet one.
     */
    static public RoutingTableEntry[] constructDirectConnection (RouterPort port) {
        if (null == port.getIp()) {
            SequenceStatus.ROUTER_PORT_NO_IP_ADDRESS.logWarning(String.format("with order %d, ID %d", port.getOrder(), port.getId()));
            return null;
        }

        int portIP = port.getIp();
        int portMask = port.getMask();

        RoutingTableEntry[] entries = new RoutingTableEntry[2];
        logger.info("Creating local port entry for port with id {}", port.getId());
        entries[0] = RoutingTableEntryFactory.createLocalPortEntry(portIP);
        entries[1] = RoutingTableEntryFactory.createConnectedSubnetEntry(portIP, portMask);

        return entries;
    }

    /**
     * Creates NEW INSTANCES of directly connected and local routes corresponding
     * to the given port, and writes them to the port's router RoutingTable.
     * @param port
     * @return HttpStatus: OK, or Implementation Warning or Error.
     */
    static public SequenceStatus routeDirectConnection (RouterPort port) {
        RouterDTO routerDTO = port.checkForOwner();
        if (null == routerDTO) {
            return SequenceStatus.ROUTER_PORT_NO_OWNER;
        }

        RoutingTable table = routerDTO.getRoutingTable();
        if (null == table) {
            SequenceStatus.ROUTER_NO_ROUTING_TABLE.logWarning(routerDTO.getIdNE());
            return SequenceStatus.ROUTER_NO_ROUTING_TABLE;
        }

        RoutingTableEntry[] entries = constructDirectConnection(port);

        table.addRoute(entries[0]);
        table.addRoute(entries[1]);

        table.prepareStringIps();
        return SequenceStatus.OK;
    }

    public static SequenceStatus routeStatic (RoutingTable table, Integer ip, Integer mask, Integer nextHop) {
        SequenceStatus routerStatus = table.ownerOK();
        if (SequenceStatus.OK != routerStatus) {
            return routerStatus;
        }

        RoutingTableEntry staticEntry = RoutingTableEntryFactory.createStaticEntry(ip, mask, nextHop);
        if (null == staticEntry) {
            logger.error("Can't create a route with ip {}, mask {}, next hop {}.", IpService.stringFromInt(ip), IpService.stringFromInt(mask), IpService.stringFromInt(nextHop));
            return SequenceStatus.UNEXPECTED_FIELD_VALUE;
        }
        table.addRoute(staticEntry);

        table.prepareStringIps();
        return SequenceStatus.OK;
    }


    @Override
    public void trackRecursively(RoutingTable instance) {
        if (null == instance) {
            SequenceStatus.NULL_POINTER.logError("trackRecursively", "instance");
            throw new NullPointerException();
        }
        if (loadedRoutingTables.contains(instance)) {
            return;
        }
        loadedRoutingTables.add(instance);
    }


    @Override
    public void untrackRecursively(long id) {
        RoutingTable instance = getLoaded(id);
        if (null == instance) {
            SequenceStatus.UNTRACKED_DB_OBJECT.logWarning("RoutingTable");
            return;
        }
        loadedRoutingTables.remove(instance);
    }

    @Override
    public void drop(RoutingTable routingTable) {
        routingTableRepository.delete(routingTable);
    }
}
