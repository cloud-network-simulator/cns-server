package com.edunetcracker.simulator.service.configurers;

import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import com.edunetcracker.simulator.model.element.switchNE.Switch;
import com.edunetcracker.simulator.model.port.SwitchPort;
import com.edunetcracker.simulator.service.NetworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SwitchConfigurer {

    Logger logger = LoggerFactory.getLogger(SwitchConfigurer.class);

    @Autowired
    NetworkService networkService;

    public void givePortsTo (SwitchDTO switchDTO) {
        assert switchDTO != null;

        if (switchDTO.getPhysConfigType() == null) {
            logger.warn("Switch with id \"{}\" didn't have a physical configuration type.", switchDTO.getIdNE());
            return;
        }

        giveLazyPortsTo(switchDTO);
    }

    /**
     * Gives 4 ports to switchEl.
     * @param switchDTO Switch to give ports to.
     */
    private void giveLazyPortsTo (SwitchDTO switchDTO) {
        assert switchDTO != null;

        List<SwitchPort> ports = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            SwitchPort nextPort = new SwitchPort();
            networkService.giveMacToPort(nextPort);
            nextPort.setSwitchDTO(switchDTO);
            nextPort.setOrder(i + 1);

            ports.add(nextPort);
        }

        switchDTO.setPorts(ports);
    }
}
