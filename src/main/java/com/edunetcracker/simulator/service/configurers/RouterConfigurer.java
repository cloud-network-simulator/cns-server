package com.edunetcracker.simulator.service.configurers;

import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.port.RouterPort;
import com.edunetcracker.simulator.service.NetworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RouterConfigurer {

    Logger logger = LoggerFactory.getLogger(RouterConfigurer.class);

    @Autowired
    NetworkService networkService;

    public void givePortsTo (RouterDTO routerDTO) {
        assert routerDTO != null;

        if (routerDTO.getPhysConfigType() == null) {
            logger.warn("Router with id \"{}\" didn't have a physical configuration type.", routerDTO.getIdNE());
            return;
        }

        giveLazyPortsTo(routerDTO);
    }

    /**
     * Gives 4 ports to router.
     * @param routerDTO Router to give ports to.
     */
    private void giveLazyPortsTo (RouterDTO routerDTO) {
        assert routerDTO != null;

        List<RouterPort> ports = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            RouterPort nextPort = new RouterPort();
            networkService.giveMacToPort(nextPort);
            nextPort.setOrder(i + 1);
            nextPort.setRouterDTO(routerDTO);

            ports.add(nextPort);
        }

        routerDTO.setPorts(ports);
    }
}
