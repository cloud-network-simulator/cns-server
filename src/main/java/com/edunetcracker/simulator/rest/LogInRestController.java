package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.DTO.UserDTO;
import com.edunetcracker.simulator.service.LogInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogInRestController {

    private final LogInService logInService;

    @Autowired
    public LogInRestController(LogInService logInService){
        this.logInService = logInService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody UserDTO user) {
        return logInService.logIn(user);
    }
}
