package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.netGraph.bCastNetGraph.SimpleBCastNetGraph;
import com.edunetcracker.simulator.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/test")
public class TestRestController extends RestControllerImpl {
    private static final Logger logger = LoggerFactory.getLogger(TestRestController.class);

    private final SceneService sceneService;
    private final RouterService routerService;
    private final SwitchService switchService;
    private final SimulatorService simulatorService;
    private final LinkService linkService;

    @Autowired
    public TestRestController(SceneService sceneService, RouterService routerService, SwitchService switchService, SimulatorService simulatorService, LinkService linkService) {
        this.sceneService = sceneService;
        this.routerService = routerService;
        this.switchService = switchService;
        this.simulatorService = simulatorService;
        this.linkService = linkService;
    }

    @RequestMapping(value = "/deletion", method = RequestMethod.GET)
    public ResponseEntity testDeletion () {
        SceneDTO sceneDTO = new SceneDTO();     sceneDTO.setName("DeletionTestScene");
        sceneService.create(sceneDTO); logger.info("testDeletion: Scene \"{}\" created with id{}", sceneDTO.getName(), sceneDTO.getId());
        sceneService.load(sceneDTO.getId()); logger.info("testDeletion: Scene loaded.");
        simulatorService.startSimulation(sceneDTO.getId()); logger.info("testDeletion: Simulation started.");

        RouterDTO router1DTO = new RouterDTO();
        router1DTO.setSceneId(sceneDTO.getId());
        router1DTO.setPhysConfigType("any");
        routerService.create(router1DTO); logger.info("testDeletion: Router1 with id{} was created.", router1DTO.getIdNE());

        RouterDTO router2DTO = new RouterDTO();
        router2DTO.setSceneId(sceneDTO.getId());
        router2DTO.setPhysConfigType("any");
        routerService.create(router2DTO); logger.info("testDeletion: Router2 with id{} was created.", router2DTO.getIdNE());

        RouterDTO router3DTO = new RouterDTO();
        router3DTO.setSceneId(sceneDTO.getId());
        router3DTO.setPhysConfigType("any");
        routerService.create(router3DTO); logger.info("testDeletion: Router3 with id{} was created.", router3DTO.getIdNE());

        SwitchDTO switch1DTO = new SwitchDTO();
        switch1DTO.setSceneId(sceneDTO.getId());
        switch1DTO.setPhysConfigType("any");
        switchService.create(switch1DTO); logger.info("testDeletion: Switch1 with id{} was created.", switch1DTO.getIdNE());

        Link link1 = new Link();
        link1.setPortAid(router1DTO.getPort(1).getId());
        link1.setPortZid(switch1DTO.getPorts().get(0).getId());
        linkService.linkComps(link1); logger.info("testDeletion: Router1:Port1 and Switch1:Port1 were linked.");

        Link link2 = new Link();
        link2.setPortAid(router2DTO.getPort(1).getId());
        link2.setPortZid(switch1DTO.getPorts().get(1).getId());
        linkService.linkComps(link2); logger.info("testDeletion: Router2:Port1 and Switch1:Port2 were linked.");

        Link link3 = new Link();
        link3.setPortAid(router2DTO.getPort(2).getId());
        link3.setPortZid(router3DTO.getPort(2).getId());
        linkService.linkComps(link3); logger.info("testDeletion: Router2:Port2 and Router3:Port2 were linked.");

        return null;
        //SetIpCommandDTO setIpCommandDTO1 = new SetIpCommandDTO(router1DTO.getIdNE(), 1, "192.1")
    }

    @RequestMapping(value = "/loading", method = RequestMethod.GET)
    public ResponseEntity testLoading () {
        SceneDTO sceneDTO = sceneService.get(1);
        RouterDTO router11 = (RouterDTO) sceneDTO.getNetworkElementDTOs().get(0);
        Router router12 = routerService.getLoaded(1);

        Router router21 = routerService.getLoaded(2);
        RouterDTO router22 = (RouterDTO) sceneDTO.getNetworkElementDTOs().get(1);

        SceneDTO sceneDTO1 = sceneDTO.getNetworkElementDTOs().get(0).getSceneDTO();
        SceneDTO sceneDTO2 = sceneDTO.getNetworkElementDTOs().get(1).getSceneDTO();
        return (ResponseEntity.ok(sceneDTO.getNetworkElementDTOs().get(0).getSceneDTO() == null));
    }

    /**
     * Tests the counting of endpoints in a broadcast network.
     *
     * @return A number of endpoints, if test was passed, or
     *         an error-describing message, if test was not passed.
     */
    @RequestMapping(value = "/bcast_endpoints_count", method = RequestMethod.GET)
    public ResponseEntity testBCastNetworkEndpointsCount () {
        SceneDTO newScene = new SceneDTO();
        newScene.setName("S22B350@Test");
        sceneService.create(newScene);
        sceneService.load(newScene.getId());

        // Putting Routers
        RouterDTO router1 = new RouterDTO();
        router1.setPhysConfigType("any"); router1.setNameNE("R1"); router1.setSceneId(newScene.getId());
        routerService.create(router1);

        RouterDTO router2 = new RouterDTO();
        router2.setPhysConfigType("any"); router2.setNameNE("R2"); router2.setSceneId(newScene.getId());
        routerService.create(router2);

        RouterDTO router3 = new RouterDTO();
        router3.setPhysConfigType("any"); router3.setNameNE("R3"); router3.setSceneId(newScene.getId());
        routerService.create(router3);

        RouterDTO router4 = new RouterDTO();
        router4.setPhysConfigType("any"); router4.setNameNE("R4"); router4.setSceneId(newScene.getId());
        routerService.create(router4);

        // Putting Switches
        SwitchDTO switch1 = new SwitchDTO();
        switch1.setPhysConfigType("any"); switch1.setNameNE("S1"); switch1.setSceneId(newScene.getId());
        switchService.create(switch1);

        SwitchDTO switch2 = new SwitchDTO();
        switch2.setPhysConfigType("any"); switch2.setNameNE("S2"); switch2.setSceneId(newScene.getId());
        switchService.create(switch2);

        // Linking first pair of routers to Switch1
        Link link1 = new Link(); link1.setSceneId(newScene.getId());
        link1.setPortAid(router1.getPorts().get(0).getId());
        link1.setPortZid(switch1.getPorts().get(0).getId());
        linkService.linkComps(link1); logger.info("testBCastNetworkEndpointsCount: Router1:Port1 and Switch1:Port1 were linked.");

        Link link2 = new Link(); link2.setSceneId(newScene.getId());
        link2.setPortAid(router2.getPorts().get(0).getId());
        link2.setPortZid(switch1.getPorts().get(1).getId());
        linkService.linkComps(link2); logger.info("testBCastNetworkEndpointsCount: Router2:Port1 and Switch1:Port2 were linked.");

        // Linking second pair of routers to Switch2
        Link link3 = new Link(); link3.setSceneId(newScene.getId());
        link3.setPortAid(router3.getPorts().get(0).getId());
        link3.setPortZid(switch2.getPorts().get(0).getId());
        linkService.linkComps(link3); logger.info("testBCastNetworkEndpointsCount: Router3:Port1 and Switch2:Port1 were linked.");

        Link link4 = new Link(); link4.setSceneId(newScene.getId());
        link4.setPortAid(router4.getPorts().get(0).getId());
        link4.setPortZid(switch2.getPorts().get(1).getId());
        linkService.linkComps(link4); logger.info("testBCastNetworkEndpointsCount: Router4:Port1 and Switch2:Port2 were linked.");

        // Double-linking the switches
        Link link5 = new Link(); link5.setSceneId(newScene.getId());
        link5.setPortAid(switch1.getPorts().get(2).getId());
        link5.setPortZid(switch2.getPorts().get(2).getId());
        linkService.linkComps(link5); logger.info("testBCastNetworkEndpointsCount: Switch1:Port3 and Switch2:Port3 were linked.");

        Link link6 = new Link(); link6.setSceneId(newScene.getId());
        link6.setPortAid(switch1.getPorts().get(3).getId());
        link6.setPortZid(switch2.getPorts().get(3).getId());
        linkService.linkComps(link6); logger.info("testBCastNetworkEndpointsCount: Switch1:Port4 and Switch2:Port4 were linked.");

        // Counting endpoints!!
        SimpleBCastNetGraph graph = new SimpleBCastNetGraph();
        graph.build(link6);
        Optional<Long> endpointsCountOpt = graph.getTaggedCount();

        sceneService.delete(newScene.getId());

        if (endpointsCountOpt.isPresent()) {
            logger.info("testBCastNetworkEndpointsCount: Number of endpoints is {}!", endpointsCountOpt.get());
            return ResponseEntity.ok().body(endpointsCountOpt.get());
        } else {
            logger.info("testBCastNetworkEndpointsCount: Graph has not been built :(");
            return ResponseEntity.ok().body("Graph has not been built :(");
        }
    }
}
