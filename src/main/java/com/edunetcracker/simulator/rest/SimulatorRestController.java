package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import com.edunetcracker.simulator.model.DTO.userCommand.PingCommandDTO;
import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.DTO.userCommand.traffic.*;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.service.*;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/simulation")
public class SimulatorRestController extends RestControllerImpl {
    private static Logger logger = LoggerFactory.getLogger(SimulatorRestController.class);

    private final SceneService sceneService;
    private final RouterService routerService;
    private final SwitchService switchService;
    private final LinkService linkService;
    private final SimulatorService simulatorService;

    @Autowired
    public SimulatorRestController(SceneService sceneService, RouterService routerService, SwitchService switchService, SimulatorService simulatorService, LinkService linkService) {
        this.sceneService = sceneService;
        this.routerService = routerService;
        this.switchService = switchService;
        this.simulatorService = simulatorService;
        this.linkService = linkService;
    }


    /**
     * Creates a new link.
     * @param link JSON with such fields:
     *             "sceneId":"",
     * 	           "portAid":"",
     * 	           "portZid":""
     * @return
     */
    @RequestMapping(value = "/link", method = RequestMethod.POST)
    public ResponseEntity linkComponents(@RequestBody Link link) {
        SequenceStatus ss = linkService.linkComps(link);
        logger.info("ConnA ok: {}", link.getConnA().getPort().getConnection() == link.getConnA());
        logger.info("ConnZ ok: {}", link.getConnZ().getPort().getConnection() == link.getConnZ());
        return ResponseEntity.ok(toJson(link));
    }

    @RequestMapping(value = "/link", method = RequestMethod.DELETE)
    public ResponseEntity deleteLink(@RequestBody Long id) {
        SequenceStatus status = linkService.deleteSynchronised(id);
        return ResponseEntity.status(status.getHttpStatus())
                .body(status.getHttpBody());
    }

    /*Returns saved state of network
     * later we can add args to load one of many saved states*/
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<List<NetworkElement>> getComponents() {
        return simulatorService.getComps();
    }


    /**
     * Starts scene simulation
     * returns OK
     */
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ResponseEntity startSimulation (@RequestBody Long sceneId) {
        return simulatorService.startSimulation(sceneId);
    }

    /**
     * Stops simulation thread.
     * @param sceneId ID of a scene which thread to stop.
     * @return Status of the operation. Read the service's method documentation for more info.
     */
    @RequestMapping(value = "/stop", method = RequestMethod.POST)
    public ResponseEntity stopSimulation (@RequestBody Long sceneId) {
        return simulatorService.stopSimulation(sceneId);
    }

    /**
     * Enables all scene components
     * returns OK
     */
    @RequestMapping(value = "/enableAll", method = RequestMethod.POST)
    public ResponseEntity startAllComponents (@RequestBody Long sceneId) {
        return simulatorService.startAllComponents(sceneId);
    }

    /**
     * Enables only stated components
     * input int[] id with id of those components which you want to start
     */
    @RequestMapping(value = "/enable", method = RequestMethod.POST)
    public ResponseEntity startComponent (@RequestBody Long idNE) {
        return simulatorService.startComponent(idNE);
    }

    /*Stops all components
     * returns OK*/
    @RequestMapping(value = "/pauseAllComponents", method = RequestMethod.POST)
    public ResponseEntity stopAllComponents(@RequestBody Long sceneId) {
        return simulatorService.pauseAllComponents(sceneId);
    }

    /*Stops only stated components
     * input int[] id with id of those components which you want to stop
     * returns OK*/
    @RequestMapping(value = "/pauseComponent", method = RequestMethod.POST)
    public ResponseEntity pauseComponent(@RequestBody Long idNE) {
        return simulatorService.pauseComponent(idNE);
    }

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity ping(@RequestBody PingCommandDTO pingCommandDTO) {
        System.out.println("ping prishol");
        //>>>>>>>>>>> Old: adding ping context by the services from the main thread
        //SequenceStatus sequenceStatus = simulatorService.ping(pingCommandDTO);
        //return ResponseEntity.ok(sequenceStatus.getHttpStatus());
        //=======================================================
        return simulatorService.addUserCommand(pingCommandDTO);
        //<<<<<<<<<<< New: adding pingContext order to the router's queue, creating ping context
        //            by router itself and from the Router Scene's thread.
    }

    @RequestMapping(value = "/fast_launch", method = RequestMethod.POST)
    public ResponseEntity fastLaunch () {
        RouterDTO router1 = new RouterDTO();
        router1.setPhysConfigType("any");
        routerService.create(router1);
        router1.getRouter().setSessionId(1L);

        RouterDTO router2 = new RouterDTO();
        router2.setPhysConfigType("any");
        routerService.create(router2);
        router2.getRouter().setSessionId(1L);

        Link link = new Link();
        link.setPortAid(router1.getPort(1).getId());
        link.setPortZid(router2.getPort(1).getId());
        linkService.linkComps(link);

        //simulatorService.startAllComponents();

        routerService.setIpAddress(router1.getIdNE(), 1, "192.168.12.1", "255.255.255.0");
        routerService.setIpAddress(router2.getIdNE(), 1, "192.168.12.2", "255.255.255.0");

        return ResponseEntity.ok(String.format("Launched routers with ids %d and %d successfully.",
                                                router1.getIdNE(), router2.getIdNE()));
    }

    //todo(dooly): write rest
    @RequestMapping(value = "/traffic/simple", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody SimpleTrafficDTO generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

    @RequestMapping(value = "/traffic/normal", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody NormalTrafficDTO generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

    @RequestMapping(value = "/traffic/uniform", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody UniformTrafficDTO generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

    @RequestMapping(value = "/traffic/poisson", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody PoissonTrafficDTO generatedConfig) {
        return simulatorService.traffic(generatedConfig);
    }

    @RequestMapping(value = "/traffic", method = RequestMethod.POST)
    public ResponseEntity traffic (@RequestBody TrafficDTO trafficDTO) {
        return simulatorService.addUserCommand(trafficDTO);
    }
}
