package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.element.router.Router;
import com.edunetcracker.simulator.model.scene.SceneExecutor;
import com.edunetcracker.simulator.service.RouterService;
import com.edunetcracker.simulator.service.SceneService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/scene")
public class SceneRestController extends RestControllerImpl {

    private final SceneService sceneService;

    @Autowired
    public SceneRestController(SceneService sceneService) {
        this.sceneService = sceneService;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ResponseEntity newScene (@RequestBody String sceneName) {
        SceneDTO newScene = new SceneDTO();
        newScene.setName(sceneName);
        SequenceStatus ss = sceneService.create(newScene);
        return ResponseEntity.ok(new Gson().toJson(newScene.getId()));
    }


    @RequestMapping(value = "/load", method = RequestMethod.GET)
    public ResponseEntity loadSceneById (@RequestParam int id) {
        SceneExecutor sceneExecutor = sceneService.load(id);
        return ResponseEntity.ok(toJson(sceneExecutor.getSceneDTO()));
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity getSceneById (@RequestParam int id){
        SceneDTO sceneDTO = sceneService.get(id);
        return ResponseEntity.ok(toJson(sceneDTO));
    }



    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity saveScene (@RequestBody Long id) {
        //ToDo: This somehow returns null!
        SceneDTO sceneDTO = sceneService.update(id);
        return ResponseEntity.ok(toJson(sceneDTO));
    }

    @RequestMapping(value = "/change", method = RequestMethod.GET)
    public ResponseEntity update (int id, SceneDTO sceneDTO) {
        SceneDTO newScene = sceneService.change(id, sceneDTO);
        return ResponseEntity.ok(toJson(newScene));
    }

    @RequestMapping(value = "/get_names", method = RequestMethod.GET)
    public ResponseEntity getSceneNames () {
        return ResponseEntity.ok(sceneService.getSceneNames());
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity removeSceneById (@RequestBody int id){
        SequenceStatus ss = sceneService.delete(id);
        return ResponseEntity.status(ss.getHttpStatus())
                             .body  (ss.getHttpBody());
    }
}
