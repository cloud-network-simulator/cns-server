package com.edunetcracker.simulator.rest;

import com.edunetcracker.simulator.model.DTO.userCommand.SetIpCommandDTO;
import com.edunetcracker.simulator.model.DTO.userCommand.IpRouteCommandDTO;
import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.service.RouterService;
import com.edunetcracker.simulator.service.SimulatorService;
import com.edunetcracker.simulator.service.configurers.RouterConfigurer;
import com.edunetcracker.simulator.service.routingService.RoutingTableService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/router")
public class RouterRestController extends RestControllerImpl {
    private static Logger logger = LoggerFactory.getLogger(RouterRestController.class);

    private final RouterService routerService;
    private final RoutingTableService routingTableService;
    private final RouterConfigurer routerConfigurer;
    private final SimulatorService simulatorService;

    @Autowired
    public RouterRestController(RouterService routerService, RoutingTableService routingTableService, RouterConfigurer routerConfigurer, SimulatorService simulatorService) {
        this.routerService = routerService;
        this.routingTableService = routingTableService;
        this.routerConfigurer = routerConfigurer;
        this.simulatorService = simulatorService;
    }

    /**
     * @param router JSON with such fields:
     * 	             "sceneId":"",
     * 	             "sessionId":"",
     * 	             "physConfigType":"any",
     * 	             "x":"",
     * 	             "y":""
     * @return Response entity with complete Router, or response entity with a correlating error.
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity addNewRouter (@RequestBody RouterDTO router){
        routerService.create(router);
        logger.info("Router created!");
        return ResponseEntity.ok(toJson(router));
    }

    /*Changes router parameters
     * input RouterDTO with current id and new parameters
     * returns JSON of an updated router*/
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity updateRouter (@RequestBody RouterDTO routerDTO){
        RouterDTO result = routerService.update(routerDTO);
        if (null == result) {
            return ResponseEntity.badRequest().body("Unable to update NE. Check backend logs for further information.");
        }
        return ResponseEntity.ok(toJson(result));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity deleteRouter (@RequestBody Long idNE) {
        SequenceStatus ss = routerService.deleteSynchronised(idNE);
        return ResponseEntity.status(ss.getHttpStatus())
                             .body  (ss.getHttpBody());
    }


    @RequestMapping(value = "/setIp", method = RequestMethod.POST)
    public ResponseEntity setIp (@RequestBody SetIpCommandDTO setIpCommandDTO) {
        //>>>>>>>>>>> Old: doing setIpAddress by the services from the main thread
        //return routerService.setIpAddress(setIpCommandDTO);
        //=======================================================
        return simulatorService.addUserCommand(setIpCommandDTO);
        //<<<<<<<<<<< New: adding a userCommand to the router's queue, doing setIpAddress by router itself
        //            and from the Router Scene's thread.
    }

    @RequestMapping(value = "/ipRoute", method = RequestMethod.POST)
    public ResponseEntity ipRoute (@RequestBody IpRouteCommandDTO ipRouteCommandDTO) {
        //>>>>>>>>>>> Old: doing ipRoute by the services from the main thread
        //RoutingTable table = routerService.routeStatic(ipRouteCommandDTO);
        //return ResponseEntity.ok(toJson(table));
        //=======================================================
        return simulatorService.addUserCommand(ipRouteCommandDTO);
        //<<<<<<<<<<< New: adding a userCommand to the router's queue, doing ipRoute by router itself
        //            and from the Router Scene's thread.
    }
}
