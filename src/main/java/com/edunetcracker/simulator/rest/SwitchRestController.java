package com.edunetcracker.simulator.rest;


import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import com.edunetcracker.simulator.service.SwitchService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/switch")
public class SwitchRestController extends RestControllerImpl {

    @Autowired
    private final SwitchService switchService;

    @Autowired
    public SwitchRestController(SwitchService switchService) {
        this.switchService = switchService;
    }

    /*Add new switch
     * input SwitchDTO (without id )
     * returns id of added switch*/
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity addNewSwitch (@RequestBody SwitchDTO switchDTO){
        SequenceStatus ss = switchService.create(switchDTO);
        return ResponseEntity.ok(toJson(switchDTO));
    }

    /*Changes switch parameters
     * input SwitchDTO with current id and new parameters
     * returns OK*/
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity update (@RequestBody SwitchDTO switchDTO){
//ToDo: Generate & return correct ResponseEntity
        SwitchDTO result = switchService.update(switchDTO);
        if (null == result) {
            return ResponseEntity.badRequest().body("Unable to update NE. Check backend logs for further information.");
        }
        return ResponseEntity.ok(toJson(switchDTO));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity deleteSwitch (@RequestBody Long idNE) {
        SequenceStatus ss = switchService.deleteSynchronised(idNE);
        return ResponseEntity.status(ss.getHttpStatus())
                .body  (ss.getHttpBody());
    }
}
