package com.edunetcracker.simulator.model.DTO.userCommand.traffic;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The input parameters for Even distribution of traffic
 */
@Getter
@Setter
@NoArgsConstructor
public class UniformTrafficDTO extends TrafficDTO {
    //minimum time between packages
    private long minTimeBet;
    //maximum time between packages
    private long maxTimeBet;

    public UniformTrafficDTO(long idNE, boolean isAlive, String destinationIP, long minTimeBet, long maxTimeBet) {
        super(idNE, isAlive, destinationIP);
        this.minTimeBet = minTimeBet;
        this.maxTimeBet = maxTimeBet;
    }
}
