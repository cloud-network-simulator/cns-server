package com.edunetcracker.simulator.model.DTO.userCommand.traffic;

import com.edunetcracker.simulator.model.DTO.userCommand.UserCommandDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * This class is created in order to keep parameters together
 * and not to send them separately each time
 * (an initialisation for Traffic)
 */
public abstract class TrafficDTO extends UserCommandDTO {
    protected boolean alive;
    protected String destinationIP;

    public TrafficDTO () {
        commandType = Type.TRAFFIC;
    }

    public TrafficDTO (long idNE, boolean alive, String destinationIP) {
        super(Type.TRAFFIC, idNE);

        this.alive = alive;
        this.destinationIP = destinationIP;
    }
}
