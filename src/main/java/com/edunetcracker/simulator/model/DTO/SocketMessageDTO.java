package com.edunetcracker.simulator.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Transient;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SocketMessageDTO {
    @Transient
    public static final String PING = "PING";
    @Transient
    public static final String TRAFFIC = "TRAFFIC";
    @Transient
    public String sessionId;
    public String type;
    public String dataId;
    public String data;

}
