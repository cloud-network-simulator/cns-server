package com.edunetcracker.simulator.model.DTO.userCommand.traffic;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SimpleTrafficDTO extends TrafficDTO {
    //time between each package is sent
    private long timeBetPacks;

    public SimpleTrafficDTO(long idNE, boolean isAlive, String destinationIP, long timeBetPacks) {
        super(idNE, isAlive, destinationIP);
        this.timeBetPacks = timeBetPacks;
    }
}
