package com.edunetcracker.simulator.model.DTO.userCommand.traffic;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class NormalTrafficDTO extends TrafficDTO {
    private double mean;
    private double standardDeviation;

    public NormalTrafficDTO(long idNE, boolean isAlive, String destinationIP, double mean,
                            double standardDeviation) {
        super(idNE, isAlive, destinationIP);
        this.mean = mean;
        this.standardDeviation = standardDeviation;
    }
}
