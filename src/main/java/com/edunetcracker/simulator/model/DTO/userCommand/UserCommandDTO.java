package com.edunetcracker.simulator.model.DTO.userCommand;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class UserCommandDTO {
    protected Type commandType;
    protected long idNE;

    public enum Type {
        PING,
        TRAFFIC,
        SET_IP,
        IP_ROUTE
    }
}
