package com.edunetcracker.simulator.model.DTO.userCommand;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IpRouteCommandDTO extends UserCommandDTO {
    private String ip;
    private String mask;
    private String nextHop;

    public IpRouteCommandDTO() {
        commandType = Type.IP_ROUTE;
    }

    public IpRouteCommandDTO(Long idNE, String ip, String mask, String nextHop) {
        super(Type.IP_ROUTE, idNE);

        this.ip = ip;
        this.mask = mask;
        this.nextHop = nextHop;
    }
}
