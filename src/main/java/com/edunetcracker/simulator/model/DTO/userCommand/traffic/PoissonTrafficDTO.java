package com.edunetcracker.simulator.model.DTO.userCommand.traffic;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PoissonTrafficDTO extends TrafficDTO {
    private long mean;

    public PoissonTrafficDTO(long idNE, boolean isAlive, String destinationIp, long mean) {
        super(idNE, isAlive, destinationIp);
        this.mean = mean;
    }
}
