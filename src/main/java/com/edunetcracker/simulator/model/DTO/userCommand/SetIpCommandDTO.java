package com.edunetcracker.simulator.model.DTO.userCommand;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class SetIpCommandDTO extends UserCommandDTO {
    Integer portNumber;
    String ip;
    String mask;

    public SetIpCommandDTO() {
        commandType = Type.SET_IP;
    }

    public SetIpCommandDTO(Long idNE, Integer portNumber, String ip, String mask) {
        super(Type.SET_IP, idNE);

        this.portNumber = portNumber;
        this.ip = ip;
        this.mask = mask;
    }
}
