package com.edunetcracker.simulator.model.DTO.userCommand;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PingCommandDTO extends UserCommandDTO {
    String destinationIp;

    public PingCommandDTO () {
        commandType = Type.PING;
    }

    public PingCommandDTO (Long idNE, String destinationIp) {
        super(Type.PING, idNE);

        this.destinationIp = destinationIp;
    }
}
