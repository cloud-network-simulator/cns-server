package com.edunetcracker.simulator.model.dataUnit.ethernet.arp;

import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;
import lombok.Getter;
import lombok.Setter;

public class ARP extends DataUnit {
    @Setter
    @Getter
    private TypeARP typeARP;

    @Getter
    private Integer sourceIp;

    @Getter
    private Integer questionedIp;

    public ARP (Integer sourceIp, Integer questionedIp, TypeARP typeARP) {
        setType(Type.ARP);
        this.sourceIp = sourceIp;
        this.questionedIp = questionedIp;
        this.typeARP = typeARP;
    }

    @Override
    public IDataUnit copy() {
        ARP newARP = new ARP(sourceIp, questionedIp, typeARP);
        getEncapsulated().ifPresent(dataUnit -> newARP.encapsulate(dataUnit.copy()));
        return newARP;
    }

    public enum TypeARP {
        REQUEST,
        REPLY
    }
}
