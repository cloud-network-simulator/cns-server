package com.edunetcracker.simulator.model.dataUnit;

import java.util.Optional;

public interface IDataUnit {

    Optional<? extends IDataUnit> getEncapsulated();

    IDataUnit copy();

    void encapsulate (IDataUnit dataUnit);

    Type getType();

    enum Type {
        None,
        Ethernet,
        ARP,
        IP,
        TCP,
        UDP,
        ICMP,
        PING        //ToDo(Wisp): PING refactoring
    }
}
