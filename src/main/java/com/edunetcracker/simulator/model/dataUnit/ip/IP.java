package com.edunetcracker.simulator.model.dataUnit.ip;

import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.model.dataUnit.IDataUnit;
import lombok.Getter;
import lombok.Setter;

public class IP extends DataUnit {
    @Setter
    @Getter
    private Integer sourceIp;

    @Setter
    @Getter
    private Integer destinationIp;

    public IP () {
        setType(Type.IP);
    }

    @Override
    public IDataUnit copy() {
        IP newIP = new IP();
        newIP.setSourceIp(sourceIp);
        newIP.setDestinationIp(destinationIp);
        newIP.setExtraData(getExtraData());
        newIP.setEncapsulatedType(getEncapsulatedType());

        getEncapsulated().ifPresent(dataUnit -> newIP.encapsulate(dataUnit.copy()));

        return newIP;
    }
}
