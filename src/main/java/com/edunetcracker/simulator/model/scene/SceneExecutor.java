package com.edunetcracker.simulator.model.scene;

import com.edunetcracker.simulator.model.DTO.SceneDTO;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.model.executor.Executee;
import com.edunetcracker.simulator.model.executor.Executor;
import com.edunetcracker.simulator.model.executor.ExecutorCompleteListener;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class SceneExecutor extends Executor {
    private static Logger logger = LoggerFactory.getLogger(SceneExecutor.class);

    @Setter
    @Getter
    private SceneDTO sceneDTO;

    @Setter
    @Getter
    private ExecutorCompleteListener<SceneExecutor> sceneService;


    public SceneExecutor () {
        sceneDTO = new SceneDTO();
    }

    public SceneExecutor (SceneDTO sceneDTO, ExecutorCompleteListener<SceneExecutor> sceneService) {
        this.sceneDTO = sceneDTO;
        this.sceneService = sceneService;
    }


    @Override
    public boolean addExecutee (Executee executee) {
        if (!(executee instanceof NetworkElement)) {
            return false;
        }
        return super.addExecutee(executee);
    }

    @Override
    protected void notifyExecutorComplete() {
        sceneService.onExecutorComplete(this);
    }

    public boolean pauseElement (long id) {
        Optional<Executee> executeeOptional = executees.stream()
                                .filter(executee -> id == ((NetworkElement)executee).getData().getIdNE())
                                .findFirst();
        executeeOptional.ifPresent(executee -> executee.setEnabled(false));
        return executeeOptional.isPresent();
    }
}
