package com.edunetcracker.simulator.model.netGraph;

import java.util.Collection;
import java.util.Optional;

public interface NetVertex {

    /**
     * Definition of being Tagged depends on an implementation.
     * If you want to know the behavior of the method, please
     * refer to the implementation.
     *
     * @return whether the Vertex is tagged.
     */
    boolean isTagged ();

    /**
     * Being a leaf means having EXACTLY one neighbour.
     *
     * @return true     if the vertex can be considered a leaf,
     *         false    otherwise.
     */
    boolean isLeaf ();

    /**
     * !Optional!
     * If implemented, returns the number of neighbours of the vertex.
     *
     * @return empty optional,                  if the method isn't implemented,
     *         optional with N of neighbours,   if it is.
     */
    Optional<Long> getNeighboursCount ();

    /**
     * !Optional!
     * If implemented, returns the iterable of NetVertex implementation.
     *
     * Please, implementing this interface, denote
     * the implementation as <code>? extends NetVertex</code></>.
     *
     * @return empty optional,                          if the method isn't implemented,
     *         optional with iterable of neighbours,    if it is.
     */
    Optional<Iterable<? extends NetVertex>> getNeighbours ();
}
