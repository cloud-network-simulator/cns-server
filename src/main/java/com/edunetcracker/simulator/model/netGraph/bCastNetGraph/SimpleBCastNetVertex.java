package com.edunetcracker.simulator.model.netGraph.bCastNetGraph;

import com.edunetcracker.simulator.model.netGraph.NetVertex;

import java.util.Optional;

/**
 * Vertices of the <code>BCastNetGraph</code>.
 * The Vertex denotes either the whole switch, or single port of the router.
 *
 * Being TAGGED equals denoting a router port.
 */
public class SimpleBCastNetVertex implements NetVertex {

    boolean leaf;
    boolean tagged;

    @Override
    public boolean isTagged() {
        return tagged;
    }

    @Override
    public boolean isLeaf() {
        return leaf;
    }

    @Override
    public Optional<Long> getNeighboursCount() {
        return Optional.empty();
    }

    @Override
    public Optional<Iterable<? extends NetVertex>> getNeighbours() {
        return Optional.empty();
    }
}
