package com.edunetcracker.simulator.model.netGraph;

import com.edunetcracker.simulator.model.DTO.element.NetworkElementDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.element.NetworkElement;
import com.edunetcracker.simulator.service.status.SequenceStatus;

import java.util.Optional;

/**
 *
 */
public interface NetGraph<VertexT> {

    /**
     * Builds a graph of a network which includes the denoted <code>Link</code>.
     *
     * @param link  The <code>Link</code> from the required network graph.
     * @return Status of the method.
     */
    SequenceStatus build (Link link);

    /**
     * Builds a graph of a network which includes the denoted <code>NetworkElement</code>.
     *
     * @param networkElementDTO  The <code>NetworkElementDTO</code> from the required network graph.
     * @return Status of the method.
     */
    SequenceStatus build (NetworkElementDTO networkElementDTO);

    /**
     * Returns the total number of nodes of the graph.
     *
     * @return Empty optional,          if the graph isn't yet built,
     *         wrapped number of nodes, if the graph is built,
     *         null,                    in case of an error.
     */
    Optional<Long> getVerticesCount ();

    /**
     * Returns the total number of Leaves (vertices-with-one-neighbour) of the graph.
     *
     * @return Empty optional,              if the graph isn't yet built,
     *         wrapped number of leaves,    if the graph is built,
     *         null,                        in case of an error.
     */
    Optional<Long> getLeavesCount ();

    /**
     * Returns the number of TAGGED vertices.
     * The meaning of being TAGGED depends on an implementation.
     *
     * @return Empty optional,                  if the graph isn't yet built,
     *         wrapped number of tagged nodes,  if the graph is built,
     *         null,                            in case of an error.
     */
    Optional<Long> getTaggedCount ();

    /**
     * Checks whether the graph has a Vertex with the given id.
     * If has, returns the Vertex.
     *
     * @param vertexId Id of the vertex.
     * @return Empty optional,                      if the graph doesn't have a vertex with the given id,
     *         wrapped Vertex with the given Id,    if does,
     *         null,                                in case of an error.
     */
    Optional<VertexT> getVertexById (long vertexId);
}
