package com.edunetcracker.simulator.model.netGraph.bCastNetGraph;

import com.edunetcracker.simulator.model.DTO.element.NetworkElementDTO;
import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import com.edunetcracker.simulator.model.Link;
import com.edunetcracker.simulator.model.netGraph.NetGraph;
import com.edunetcracker.simulator.model.port.Port;
import com.edunetcracker.simulator.model.port.RouterPort;
import com.edunetcracker.simulator.model.port.SwitchPort;
import com.edunetcracker.simulator.service.status.SequenceStatus;

import java.util.HashMap;
import java.util.Optional;

/**
 * Simple graph built to count a number of end-points of a given broadcast network.
 * The vertices(nodes) are either whole Switches, or single Router Ports.
 *
 * The graph is represented as an adjacency table saved as a <code>HashMap</code>, with
 *  - keys of <b>Router Port</b>-vertices being their ids, and
 *  - keys of <b>Switch</b>-vertices being their idNEs <b>multiplied by "-1"</b>,
 * so that the ids do not intersect.
 *
 * !ATTENTION!
 * This graph does not check whether the network has several ports of THE SAME router inside it.
 */
public class SimpleBCastNetGraph implements NetGraph<SimpleBCastNetVertex> {

    /**
     * The Adjacency Table.
     * Keys of <b>Router Port</b>-vertices are their MAC-addresses.
     * Keys of <b>Switch</b>-vertices are their idNEs.
     */
    private HashMap<Long, SimpleBCastNetVertex> nodesMap;

    private long taggedCount;
    private long leavesCount;

    public SimpleBCastNetGraph() {
        nodesMap = new HashMap<>();
        taggedCount = 0;
        leavesCount = 0;
    }

    private SequenceStatus addRecursively (Port port) {
        SimpleBCastNetVertex newVertex = new SimpleBCastNetVertex();

        if (port instanceof RouterPort) {
            if (!nodesMap.containsKey(port.getId())) {
                newVertex.leaf = true;
                newVertex.tagged = true;
                nodesMap.put(port.getId(), newVertex);
                taggedCount++;
                leavesCount++;
            }
        } else if (port instanceof SwitchPort) {
            SwitchDTO switchDTO = ((SwitchPort) port).getSwitchDTO();
            if (!nodesMap.containsKey(-1 * switchDTO.getIdNE())) {
                newVertex.tagged = false;
                newVertex.leaf = switchDTO.getPorts().stream()
                        .filter(switchPort -> switchPort.getConnection() != null)
                        .count() == 1;
                nodesMap.put(-1 * switchDTO.getIdNE(), newVertex);
                if (newVertex.leaf) { leavesCount++; }

                switchDTO.getPorts().stream()
                        .filter(switchPort -> switchPort.getConnection() != null)
                        .map(switchPort -> switchPort.getConnection().getOppositeConn().getPort())
                        .forEach(this::addRecursively);
            }
        }

        return SequenceStatus.OK;
    }

    @Override
    public SequenceStatus build(Link link) {
        nodesMap.clear();
        SequenceStatus ss1 = addRecursively(link.getConnA().getPort());
        SequenceStatus ss2 = addRecursively(link.getConnZ().getPort());
        if (ss1 != SequenceStatus.OK) {
            return ss1;
        }
        return ss2;
    }

    @Override
    public SequenceStatus build(NetworkElementDTO networkElementDTO) {
        nodesMap.clear();
        if (!(networkElementDTO instanceof SwitchDTO)) {
            SequenceStatus.PARAMETER_TYPE_INCONSISTENCY.logError("SwitchDTO");
            return SequenceStatus.PARAMETER_TYPE_INCONSISTENCY;
        }
        return addRecursively(((SwitchDTO)networkElementDTO).getPorts().get(0));
    }

    @Override
    public Optional<Long> getVerticesCount() {
        if (nodesMap.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of((long)nodesMap.size());
        }
    }

    @Override
    public Optional<Long> getLeavesCount() {
        if (nodesMap.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(leavesCount);
        }
    }

    @Override
    public Optional<Long> getTaggedCount() {
        if (nodesMap.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(taggedCount);
        }
    }

    @Override
    public Optional<SimpleBCastNetVertex> getVertexById(long vertexId) {
        if (nodesMap.containsKey(vertexId)) {
            return Optional.of(nodesMap.get(vertexId));
        }
        if (nodesMap.containsKey(-vertexId)) {
            return Optional.of(nodesMap.get(-vertexId));
        }
        return Optional.empty();
    }
}
