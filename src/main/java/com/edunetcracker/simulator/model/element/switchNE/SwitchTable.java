package com.edunetcracker.simulator.model.element.switchNE;

import java.util.HashMap;
import java.util.Map;

//ToDo(Wisp): Base (maybe abstract) class for SwitchTable or ARPTable.
public class SwitchTable {

    private Map<Long, Entry> entries;

    public SwitchTable () {
        entries = new HashMap<>();
    }

    public Integer getPortNumber (Long destinationMAC) {
        Entry entry = entries.get(destinationMAC);
        return (null == entry) ? null : entry.portNumber;
    }

    public void addEntry (Long destinationMAC, Integer portNumber) {
        Entry entry = new Entry(portNumber, destinationMAC);
        entries.put(destinationMAC, entry);
    }

    public void onTick () {
        entries.values().removeIf(nextEntry -> --nextEntry.ticksToLive <= 0);
    }

    private class Entry {
        private final int DEFAULT_TICKS_TO_LIVE = 1024;

        private Integer portNumber;
        private Long destinationMAC;
        private int ticksToLive;

        private Entry (Integer portNumber, Long destinationMAC) {
            this.portNumber = portNumber;
            this.destinationMAC = destinationMAC;
            ticksToLive = DEFAULT_TICKS_TO_LIVE;
        }
    }
}
