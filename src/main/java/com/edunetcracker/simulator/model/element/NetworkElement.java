
package com.edunetcracker.simulator.model.element;

import com.edunetcracker.simulator.model.DTO.SocketMessageDTO;
import com.edunetcracker.simulator.model.DTO.element.NetworkElementDTO;
import com.edunetcracker.simulator.model.DTO.userCommand.UserCommandDTO;
import com.edunetcracker.simulator.model.context.NEContext;
import com.edunetcracker.simulator.model.executor.Executee;
import com.edunetcracker.simulator.model.scene.SceneExecutor;
import com.edunetcracker.simulator.service.WSService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

@Setter
@Getter
//public abstract class NetworkElement implements DBObject<NetworkElement>, Executee {
public abstract class NetworkElement extends Executee<SceneExecutor> {
    private static Logger logger = LoggerFactory.getLogger(NetworkElement.class);

    private NetworkElementDTO data;

    protected List<NEContext> contexts;

    private float loading;

    private float averageLoading;

    private int statusSendingCounter;

    protected final static int inputProcessingRate = 100; //maximum amount packages per tact
    protected final static int statusSendingPeriod = 5;  //status is sending 1 tact per statusSendingRate
    protected final static int loadingRate = 100;

    //ToDo: Rework connection with front
    protected Long sessionId = 111L;

    //tasks from user
    private ConcurrentLinkedQueue<UserCommandDTO> userInput;
    private ConcurrentLinkedQueue userOutput;

    private Long stamp;

    public NetworkElement () {
        contexts = new LinkedList<>();
        statusSendingCounter = 0;
        userInput = new ConcurrentLinkedQueue<>();
        userOutput = new ConcurrentLinkedQueue<>();
    }

    protected NetworkElement (NetworkElementDTO data) {
        this.data = data;
        contexts = new LinkedList<>();
        statusSendingCounter = 0;
        userInput = new ConcurrentLinkedQueue<>();
        userOutput = new ConcurrentLinkedQueue<>();
    }


    @Override
    public void awake() {}

    @Override
    public void start () {
        statusSendingCounter = 0;
        averageLoading = 0;
        stamp = System.currentTimeMillis() - 200;
    }

    @Override
    public void run() {
        if (0 < 100 + stamp - System.currentTimeMillis())
        { return; }

        statusSendingCounter++;
        averageLoading = averageLoading + loading;

        processCommands();
        processInputTraffic();
        processContexts();

        stamp = System.currentTimeMillis();

        if (statusSendingCounter == statusSendingPeriod) {
            sendStatus();
        }
    }

    @Override
    public void onDeletion () {
        //ToDo: NetworkElement.OnDeletion()
    }

    private void sendStatus () {
        String load = Integer.toString(Math.round(averageLoading * 100 / statusSendingPeriod));
        SocketMessageDTO message = new SocketMessageDTO();

        message.setSessionId("111");
        message.setType(SocketMessageDTO.TRAFFIC);
        message.setDataId(Long.toString(data.getIdNE()));
        message.setData(load);
        message = new SocketMessageDTO("111", SocketMessageDTO.TRAFFIC, Long.toString(data.getIdNE()), load);
        WSService.getInstance().send(message);

        statusSendingCounter = 0;
        averageLoading = 0;
    }

    protected abstract void processContexts();

    protected abstract void processInputTraffic();

    protected abstract void processCommands();

    protected void evaluateLoading (int processedDUnitsNumber) {
        setLoading((float)processedDUnitsNumber/(float)inputProcessingRate);
        logger.trace("Router with id {}: processed {} dataunits, loading is {}", data.getIdNE(), processedDUnitsNumber, loading);
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibilityChecker(mapper.getDeserializationConfig().getDefaultVisibilityChecker()
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE)
                .withFieldVisibility(JsonAutoDetect.Visibility.NONE)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE));
        String result = null;
        try {
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

}


