package com.edunetcracker.simulator.model.port;

import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import com.edunetcracker.simulator.model.dataUnit.DataUnit;
import com.edunetcracker.simulator.service.routingService.IpService;
import com.edunetcracker.simulator.service.status.SequenceStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.persistence.*;

@Getter
@Entity
public class RouterPort extends Port {
    @Transient
    private static Logger logger = LoggerFactory.getLogger(RouterPort.class);


    @Setter
    @Column(name = "ip")
    @JsonProperty
    private Integer ip;

    @Setter
    @Column(name = "mask")
    @JsonProperty
    private Integer mask;

    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "router", referencedColumnName = "idne")
    private RouterDTO routerDTO;


    @Override
    public SequenceStatus copyRefs(Port another) {
        if (!(another instanceof RouterPort)) {
            SequenceStatus.PARAMETER_TYPE_INCONSISTENCY.logError("RouterPort");
            return SequenceStatus.PARAMETER_TYPE_INCONSISTENCY;
        }
        super.copyRefs(another);
        RouterPort anotherRP = (RouterPort)another;
        routerDTO = anotherRP.routerDTO;
        return SequenceStatus.OK;
    }

    public void setIpAddress (int ip, int mask) {
        this.ip = ip;
        this.mask = mask;
    }

    public SequenceStatus setIpAddress (String ip, String mask) {
        if (null == ip) {
            SequenceStatus.NULL_POINTER.logError("setIpAddress", "String ip");
            return SequenceStatus.NULL_POINTER;
        }
        int intIp = IpService.intFromString(ip);
        int intMask = IpService.intFromString(mask);
        setIpAddress(intIp, intMask);
        return SequenceStatus.OK;
    }

    @Override
    public RouterDTO checkForOwner () {
        if (null == routerDTO) {
            SequenceStatus.ROUTER_PORT_NO_OWNER.logWarning(getId());
        }
        return routerDTO;
    }

    @Override
    protected DataUnit rejectedDueToOverflow(DataUnit dataUnit) {
        //ToDo(Wisp): Implement router behaviour on port overflow
        throw new NotImplementedException();
    }


}
