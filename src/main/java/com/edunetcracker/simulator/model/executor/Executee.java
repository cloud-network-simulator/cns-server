package com.edunetcracker.simulator.model.executor;

import lombok.Getter;
import lombok.Setter;

/**
 * Assigned to an extension of <code>Executor</code>,
 * an extension of Executee will have its methods called on certain conditions.
 */
public abstract class Executee<E extends Executor> implements Comparable<Executee> {

    @Getter
    E executor;

    /**
     * The disabled <code>Executee</code> will remain in the <code>Executor</code>'s
     * Executee list, but the <code>run</code> method will not be called until it is enabled again.
     */
    @Getter
    @Setter
    protected boolean enabled;

    @Getter
    protected boolean deleted;

    @Getter
    protected boolean started;

    protected Executee () {
        enabled = true;
        started = false;
        deleted = false;
    }

    /**
     * Called in the very beginning of an Executor thread.
     * This method WILL be called in the middle of lifecycle by the <code>Executor</code>, if has never been before.
     * ORDER OF OBJECTS TO <code>awake</code> IS UNDEFINED!
     */
    protected abstract void awake();

    /**
     * Called after the initialization of each and every Executee belonging to the Executor.
     * --> On Executor's thread launching, called after all its Executees' "awakes"
     * --> In the middle of a lifecycle, called right after own "awake".
     * This method WILL be called in the middle of lifecycle by the <code>Executor</code>, if has never been before.
     * ORDER OF OBJECTS TO <code>start</code> IS UNDEFINED!
     */
    protected abstract void start();

    /**
     * Called in every loop of the <code>Executor</code> lifecycle.
     */
    protected abstract void run();

    /**
     * Called on deletion of the <code>Executee</code> from
     * the <code>Executor</code>'s Executee list.
     */
    protected abstract void onDeletion();

    /**
     * Tells the <code>Executor</code> that this Executee wants to be deleted.
     */
    public final void delete() {
        deleted = true;
    }

    /**
     * Method required for realisation of comparators.
     *
     * @return
     */
    public abstract long getExecuteeId();

    /**
     * Comparator required by the {@code Executor}'s TreeSet of {@code Executee}s.
     *
     * @param another {@code Executee} to compare to.
     * @return
     */
    public abstract int compareTo (Executee another);
}
