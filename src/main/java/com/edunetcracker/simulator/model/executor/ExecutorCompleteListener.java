package com.edunetcracker.simulator.model.executor;

public interface ExecutorCompleteListener<E extends Executor> {
    void onExecutorComplete (E executor);
}
