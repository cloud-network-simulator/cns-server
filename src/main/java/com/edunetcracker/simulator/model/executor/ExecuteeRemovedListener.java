package com.edunetcracker.simulator.model.executor;

public interface ExecuteeRemovedListener {
    void onExecuteeRemoved (Executee executee);
}
