package com.edunetcracker.simulator.model.executor;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Executors are operating the extensions of <code>Executee</code> class:
 * their methods such as <code>start</code>, <code>run</code>,
 * <code>death</code>, e.t.c. are called by the Executor.
 */
public abstract class Executor extends Thread {
    private final static Logger logger = LoggerFactory.getLogger(Executor.class);


    @AllArgsConstructor
    protected static class RemovedExecuteeDTO {
        Executee executee;
        ExecuteeRemovedListener listener;
    }


    @Getter
    private boolean started;
    private boolean running;

    protected ConcurrentLinkedQueue<Executee> newExecuteeQueue;
    protected ConcurrentLinkedQueue<RemovedExecuteeDTO> removedExecuteesQueue;



    protected Set<Executee> executees;

    //ToDo: !!!Comparator for executees!!!
    public Executor() {
        executees = new TreeSet<>();
        newExecuteeQueue = new ConcurrentLinkedQueue<>();
        removedExecuteesQueue = new ConcurrentLinkedQueue<>();
    }

    /**
     * Adds new executee to this executor thread.
     *
     * If the thread is running, puts the executee into a queue to be added later in
     * the end of the lifecycle loop.
     *
     * Else, if the thread is not running, puts the executee directly into
     * the executee list.
     *
     * @param executee Executee to be added into executee list
     * @return True,  if was added into a queue;
     *         False, if was not.
     */
    public boolean addExecutee (Executee executee) {
        if (running) {
            return newExecuteeQueue.add(executee);
        } else {
            executee.executor = this;
            return executees.add(executee);
        }
    }

    /**
     *
     * @param executee
     * @param listener
     * @return True,  if the thread is running and the removal is postponed to queue;
     *         False, if the thread is not running and executee is removed.
     */
    public boolean removeExecutee (Executee executee, ExecuteeRemovedListener listener) {
        if (running) {
            RemovedExecuteeDTO removedExecuteeDTO = new RemovedExecuteeDTO(executee, listener);
            removedExecuteesQueue.add(removedExecuteeDTO);
            return false;
        } else {
            executees.remove(executee);
            executee.executor = null;
            return true;
        }
    }

    /**
     * Calls awake() and start() of all {@code Executees}s,
     * then calls Thread.start()
     */
    @Override
    public void start() {
        for (Executee executee : executees) {
            if (executee.started) {
                logger.error("!!! Noticed STARTED executee on an initialization.");
            }
            executee.awake();
        }
        for (Executee executee : executees) {
            if (executee.enabled) {
                executee.start();
                executee.started = true;
            }
        }

        started = true;
        running = true;
        super.start();
    }

    /**
     * Thread.run() implementation.
     */
    @Override
    public void run () {
        logger.info("Thread is now up and running!");
        while (running) {
            for (Executee executee: executees) {
                if (!executee.started) {
                    executee.awake();
                    executee.start();
                    executee.started = true;
                }
                if (executee.enabled) {
                    executee.run();
                }
            }

            addQueuedExecutees();
            removeQueuedExecutees();
        }

        notifyExecutorComplete();
    }

    private void addQueuedExecutees() {
        Executee newExecutee = null;
        while (null != (newExecutee = newExecuteeQueue.poll())) {
            newExecutee.executor = this;
            executees.add(newExecutee);
        }
    }

    private void removeQueuedExecutees() {
        RemovedExecuteeDTO executeeToRemove = null;
        while (null != (executeeToRemove = removedExecuteesQueue.poll())) {
            executees.remove(executeeToRemove.executee);
            executeeToRemove.executee.executor = null;
            notifyExecuteeRemoved(executeeToRemove);
        }
    }

    /**
     * Method to be called to <i>stop</i> the Executor.
     */
    public void lastLoop () {
        //ToDo: Finalize Executor thread.
        running = false;
    }

    /**
     * Discards all of the executees from execution list.
     * (Doesn't delete them, just tells not to be executed)
     */
    public void pauseAll () {
        if (!running) {
            return;
        }

        for (Executee executee : executees) {
            executee.setEnabled(false);
        }
    }

    public void resumeAll () {
        if (!running) {
            return;
        }

        for (Executee executee : executees) {
            executee.setEnabled(true);
        }
    }


    /**
     * Tell listeners that the executor
     * has finished its last loop.
     */
    protected abstract void notifyExecutorComplete ();

    /**
     * Tells the {@code RemovedExecuteeListener} defined in {@code RemovedExecuteeDTO}
     * that the {@code Executor} has removed {@code removedExecutee} from its executee list.
     *
     * @param removedExecuteeDTO Recently removed Executee and the Listener which asked it to be removed.
     */
    private void notifyExecuteeRemoved (RemovedExecuteeDTO removedExecuteeDTO) {
        removedExecuteeDTO.listener.onExecuteeRemoved(removedExecuteeDTO.executee);
    }
}
