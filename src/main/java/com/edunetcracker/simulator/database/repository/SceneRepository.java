package com.edunetcracker.simulator.database.repository;

import com.edunetcracker.simulator.model.DTO.SceneDTO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SceneRepository extends CrudRepository<SceneDTO, Long> {

// Methods to find a set of objects

    List<SceneDTO> findAll ();
//ToDo: Finding scenes by user
// List<Scene> findAllByUser (User user);


// Methods to find a single object

    Optional<SceneDTO> findById (Long id);
    Optional<SceneDTO> findByName (String name);


// Methods to save / remove object(s)

    SceneDTO save (SceneDTO entity);

    void deleteById (Long id);
    void deleteByName (String name);

//ToDo: deleting scenes by user
//  void deleteAllByUser(User user);
}
