package com.edunetcracker.simulator.database.repository.networkElementRepository;

import com.edunetcracker.simulator.model.DTO.element.SwitchDTO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SwitchRepository extends CrudRepository<SwitchDTO, Long> {

// Methods to find a set of objects

    List<SwitchDTO> findAll();


// Methods to find a single object

    Optional<SwitchDTO> findByXAndY(Long x, Long y);
    Optional<SwitchDTO> findByIdNE(Long id);


// Methods to save / remove object(s)

    SwitchDTO save (SwitchDTO entity);

    void deleteByIdNE (Long idNE);
}
