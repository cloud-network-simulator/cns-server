package com.edunetcracker.simulator.database.repository.networkElementRepository;

import com.edunetcracker.simulator.model.DTO.element.RouterDTO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RouterRepository extends CrudRepository<RouterDTO, Long> {

// Methods to find a set of objects

    List<RouterDTO> findAll();
    //List<Router> findAllBySceneId(Long sceneId);


// Methods to find a single object

    Optional<RouterDTO> findByXAndY(long x, long y);
    Optional<RouterDTO> findByIdNE(Long id);


// Methods to save / remove object(s)

    RouterDTO save (RouterDTO entity);

    void deleteByIdNE (Long id);

    //void deleteAllBySceneId(Long sceneId);
}
